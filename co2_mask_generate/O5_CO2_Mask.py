# %%
import numpy as np
import matplotlib.pyplot as plt
import finesse
import h5py
from scipy import signal
from scipy.interpolate import RegularGridInterpolator, CloughTocher2DInterpolator
from finesse.knm.maps import Map

finesse.init_plotting()

# %%

f = h5py.File("./O5_optimial_CO2_but_not_really.h5")
co2_profile = f["ITM/TCS/ACT/CO2/PROFILE"]
r_in = co2_profile["Rcoords"][()].squeeze()
co2_I = co2_profile["Zdata"][()].squeeze()
co2_surf_hr = f["ITM/TCS/ACT/CO2/SURF_HR"]
co2_sub = f["ITM/TCS/ACT/CO2/SUB"]
r_co2_surf_hr = co2_surf_hr["Rcoords"][()].squeeze()
co2_surf_hr_deform = co2_surf_hr["Zdata"][()].squeeze()
r_co2_sub = co2_sub["Rcoords"][()].squeeze()
co2_sub_opd = co2_sub["Zdata"][()].squeeze()


# Finiding the best Gaussian profile to use for generating  a gaussian
def gauss_r(r, P, w0):
    """Function to generate a radial profile of a gaussian beam."""
    I0 = 2 * P / np.pi / w0**2
    return I0 * np.exp(-2 * r**2 / w0**2)


def find_P(r, w0):
    P_test = 1
    _gauss = gauss_r(r, P_test, w0)
    while all(_gauss > co2_I) is False:
        P_test += 0.02
        _gauss = gauss_r(r, P_test, w0)
    return P_test


# %%
# Compute total power under the curve:
dr = r_in[1] - r_in[0]
p_co2_inc = 2 * np.pi * (co2_I * r_in * dr).sum()

# compute efficiency:
w0s = np.linspace(0.1, 0.5, 60)
p_w0s = []
for _w0 in w0s:
    p_w0s.append(find_P(r_in, _w0))
p_w0s = np.array(p_w0s)
efficiency = 100 * (p_co2_inc / p_w0s)

plt.plot(w0s, efficiency)
plt.xlabel("beam size [m]")
plt.ylabel("efficiency [%]")
print(f"Best beam size: {w0s[np.argmax(efficiency)]:.3f}")

# %%
wGauss_opt = w0s[np.argmax(efficiency)]  # Best Gaussian beam radius to use at ITM
p_wGauss_opt = p_w0s[
    np.argmax(efficiency)
]  # Corresponding power of Gaussian beam radius to use at ITM

_x = _y = np.linspace(-r_in.max(), r_in.max(), 1064)
_xx, _yy = np.meshgrid(_x, _y)
_rr = np.sqrt(_xx**2 + _yy**2)
_theta = np.arctan2(_yy, _xx)
# Interpolate co2_I to 2D intensity:
co2_I_2d = np.interp(_rr, r_in, co2_I)
# %%
# band-pass filter:
fs = 1 / (r_in[1] - r_in[0])
f, Pxx_I_co2 = signal.welch(co2_I, fs, "hann", 128)
# find peaks
peaks, _ = signal.find_peaks(np.log10(Pxx_I_co2), height=-5)

# band-pass:
df = 5
lowcut = f[peaks[-1]] - df
highcut = f[peaks[-1]] + df
b, a = signal.iirnotch(f[peaks[-1]], Q=4, fs=fs)
co2_I_filtered = signal.lfilter(b, a, co2_I)
co2_I_filtered_2d = np.interp(_rr, r_in, co2_I_filtered)


# %%
# Limit the extent of the mask:
threshold = 0.1
co2_I_filtered_2d[co2_I_filtered_2d < threshold] = 0
plt.imshow(co2_I_filtered_2d)
# Get the 2D continuous mask:
mask_2d_cont = co2_I_filtered_2d / gauss_r(_rr, p_wGauss_opt + 0.1, wGauss_opt)
mask_2d_cont /= mask_2d_cont.max()
plt.imshow(mask_2d_cont)
plt.colorbar()


# %%
# Number of leaves and maximum lead diamter
N_leaves = 8
od_max = 0.9
ring_thickness = 2e-3

max_R = np.max(r_in)
co2_I_dummy = co2_I_filtered.copy()
co2_I_dummy[(r_in < 0.03) | (r_in > 0.14)] += 10
r1 = r_in[np.argmin(co2_I_dummy)] - 0e-3
# get the rings:
# r1 = 0.12
r2 = 0.08
r3 = 0.04
r4 = 0.02
theta_A = (
    _theta * ((_rr > (r1 + ring_thickness / 2)) * 3)
    + _theta
    * ((_rr <= (r1 - ring_thickness / 2)) * (_rr > (r2 + ring_thickness / 2)) * 3)
    + _theta
    * ((_rr <= (r2 - ring_thickness / 2)) * (_rr > (r3 + ring_thickness / 2)) * 2)
    + _theta
    * ((_rr <= (r3 - ring_thickness / 2)) * (_rr > (r4 + ring_thickness / 2)) * 1)
    + _theta * ((_rr <= (r3 - ring_thickness / 2)) * 0.5)
)

drR = 2  # raial scaling factor

theta_Sc = (
    (_rr < r4 - ring_thickness * drR)
    + (1 + 0.5 / drR)
    * ((_rr >= r4 - ring_thickness * drR) * (_rr < r4 + ring_thickness * drR))
    + (_rr >= r4 + ring_thickness * drR) * (_rr < r3 - ring_thickness * drR)
    + (1 + 0.5 / drR)
    * ((_rr >= r3 - ring_thickness * drR) * (_rr < r3 + ring_thickness * drR))
    + (_rr >= r3 + ring_thickness * drR) * (_rr < r2 - ring_thickness * drR)
    + (1 + 0.5 / drR)
    * ((_rr >= r2 - ring_thickness * drR))
    * (_rr < r2 + ring_thickness * drR)
    + (_rr >= r2 + ring_thickness * drR) * (_rr < r1 - ring_thickness * drR)
    + (1 + 0.5 / drR)
    * (_rr >= r1 + ring_thickness * drR)
    * (_rr < r1 + ring_thickness * drR)
    + (_rr >= r1 + ring_thickness * drR)
)

theta2 = N_leaves * theta_A / (np.pi)
theta2 = np.mod(theta2, 1) - 0.5

# Generate mask
mask_leaves = theta2 < mask_2d_cont * theta_Sc / 2.1
mask_leaves = mask_leaves * (
    theta2 >= -mask_2d_cont * theta_Sc / 2.1
)  # Here 2.1 is use instead of 2 to ensure the mask spokes are sufficiently thick for fabrication
mask_leaves *= _rr < 0.162
mask_leaves *= _rr > 0.01

plt.figure(figsize=(10, 6))
plt.contourf(_x, _y, mask_leaves)
plt.gca().set_aspect("equal")
# %%
gauss_masked = gauss_r(_rr, p_wGauss_opt + 0.1, wGauss_opt) * mask_leaves
plt.figure(figsize=(10, 6))
plt.contourf(_x, _y, gauss_masked, 20)
plt.gca().set_aspect("equal")
plt.colorbar()
# %%
# Compute azimuthal average
_rp = np.linspace(0, r_in.max(), 512)
_theta_p = np.linspace(0, 2 * np.pi, 512)
_rrp, _ttp = np.meshgrid(_rp, _theta_p)
_xxp = _rrp * np.cos(_ttp)
_yyp = _rrp * np.sin(_ttp)

interp_mask = RegularGridInterpolator((_x, _y), gauss_masked)
gaussed_masked_p = interp_mask((_xxp, _yyp))
gaussed_masked_p_mean = gaussed_masked_p.mean(axis=0)
# %%
plt.plot(_rp, gaussed_masked_p_mean, label="azimuthal mask avergae")
plt.plot(
    r_in, gauss_r(r_in, p_wGauss_opt + 0.1, wGauss_opt), label="illuminating Gaussian"
)
plt.plot(r_in, co2_I_filtered, label="idealised CO2 profile")
plt.xlabel("radius [m]")
plt.ylabel(r"Intensity [W/m$\mathrm{^2}$]")

# %%
# Save masked intensity as  image to use for FEM
fig = plt.figure(frameon=False, figsize=(10, 10))
plt.axis("off")
plt.imshow(gauss_masked, aspect="equal")
plt.savefig("./co2_mask_data/o5_co2_masked_gaussian.png", dpi=300)

# %%
# Loading results from FEA study
fem_file = np.loadtxt("./co2_mask_data/o5_co2_mask_fem_results_v0.txt", comments="%")
x_fem = fem_file[:, 0]
y_fem = fem_file[:, 1]

fig, axs = plt.subplots(1, 3, figsize=(12, 3))

for ax, _map, _label in zip(
    axs, fem_file[:, 3:].T, ("HR deformation", "ITM OPD", "CP OPD")
):
    if _label == "CP OPD":
        _map[_map < -30e-9] = -30e-9
    cf = ax.tricontourf(x_fem, y_fem, 1e9 * _map, 20)
    ax.set_aspect("equal")
    ax.set_title(_label)
    cb = fig.colorbar(cf, ax=ax)
    cb.ax.set_ylabel("[nm]")
    ax.set_xlabel("x [m]")
    ax.set_ylabel("y [m]")
plt.tight_layout()
plt.savefig("./co2_mask_figures/co2_mask_1W_optical_response.pdf")
# %%
# interpolate over a regular grid
x_tm_1d = y_tm_1d = np.linspace(-0.17, 0.17, 201)
xx_tm, yy_tm = np.meshgrid(x_tm_1d, y_tm_1d)

map_interpolators = []
opd_maps = []

for _map in fem_file[:, 3:].T:
    interpolator = CloughTocher2DInterpolator(list(zip(x_fem, y_fem)), _map)
    map_interpolators.append(interpolator)
    _map_interp = interpolator(xx_tm, yy_tm)
    opd_maps.append(_map_interp)
# %%
#  Extract defocus from deformation map
deform_map = Map(
    x_tm_1d, y_tm_1d, amplitude=(~np.isnan(opd_maps[0])), opd=np.nan_to_num(opd_maps[0])
)
roc_x, roc_y = deform_map.get_radius_of_curvature_reflection(53e-3)
Sx_deform = 2 / roc_x
Sy_deform = 2 / roc_y
print(f"Surface deformation defocus:({1e6 * Sx_deform:.3f}, {1e6 * Sy_deform:.3f}) uD")

# %%
# Extract defocus from opd map
total_opd = opd_maps[1] + opd_maps[2]
opd_map = Map(
    x_tm_1d, y_tm_1d, amplitude=(~np.isnan(total_opd)), opd=np.nan_to_num(total_opd)
)
f_x, f_y = opd_map.get_thin_lens_f(53e-3)
Sx_opd = 1 / f_x
Sy_opd = 1 / f_y
print(f"OPD defocus:({1e6 * Sx_opd:.3f}, {1e6 * Sy_opd:.3f}) uD")

# %%
# Loading in realistic RH to save together:
fem_file_rh = np.loadtxt("./co2_mask_data/RH_ITM_realistic1W.txt", comments="%")
x_fem_rh = fem_file_rh[:, 0]
y_fem_rh = fem_file_rh[:, 1]

rh_map_interpolators = []
rh_opd_maps = []
for _map in fem_file_rh[:, 3:].T:
    interpolator = CloughTocher2DInterpolator(list(zip(x_fem_rh, y_fem_rh)), _map)
    rh_map_interpolators.append(interpolator)
    _map_interp = interpolator(xx_tm, yy_tm)
    rh_opd_maps.append(_map_interp.T)

# %%
# Export data:
o5_co2_mask_data = {}
o5_co2_mask_data["x_mask"] = _x
o5_co2_mask_data["y_mask"] = _y
o5_co2_mask_data["co2_mask"] = mask_leaves
o5_co2_mask_data["co2_gaussian_beamsize"] = wGauss_opt
o5_co2_mask_data["co2_out1W_incident_power"] = (p_wGauss_opt + 0.1) / np.sum(
    gauss_masked * (_x[1] - _x[0]) ** 2
)
o5_co2_mask_data["x_map"] = x_tm_1d
o5_co2_mask_data["y_map"] = y_tm_1d
o5_co2_mask_data["co2_hr_surf_deformation"] = opd_maps[0]
o5_co2_mask_data["co2_sub_opd"] = total_opd
o5_co2_mask_data["rh_hr_surf_deformation"] = rh_opd_maps[0]
o5_co2_mask_data["rh_sub_opd"] = rh_opd_maps[1] + rh_opd_maps[2]

np.savez("./co2_mask_data/o5_co2_mask_data_v0.npz", **o5_co2_mask_data)

# %%
