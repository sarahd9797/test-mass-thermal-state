import glob
import h5py

# Get a list of all .h5 files in a directory
files = glob.glob('./TS_*.h5')


with h5py.File('combined_TS.h5', 'w') as f_out:
    # Loop over your list of files
    for file in files:
        # Open the file
        with h5py.File(file, 'r') as f:
            # Loop through the groups in the file and copy to the output file
            for group in f.keys():
                f.copy(group, f_out)
