from numpy import ndarray
from rombus.model import RombusModel
from typing import NamedTuple
import numpy as np
import h5py

_h = h5py.File(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/combined_TS.h5",
    "r",
)
_t = _h.get("4/deformation/deformation_data")
_mask_data = _h.get("4/deformation/mask_data")

_z_surface1_masked = _t[:][:, :, 0, 2][_mask_data[:, :, 0]]
_z_surface2_masked = _t[:][:, :, 1, 2][_mask_data[:, :, 1]]
_z_surface_both_masked = np.hstack((_z_surface1_masked, _z_surface2_masked))


class Model(RombusModel):
    # NOTE: these are masked, raveled (1D) coordinates
    coordinate.set(
        "x",
        0.0,
        float(len(_z_surface1_masked) - 1),
        len(_z_surface1_masked),
        label="$x$",
    )

    ordinate.set("y", label="$y(x)$")

    params.add("idx", 0, 10000)

    def compute(self, p: NamedTuple, x: ndarray) -> ndarray:
        """Compute the model for a given parameter set."""
        _s = _h.get("%i/deformation/deformation_data" % (p.idx))
        _s1 = _s[:][:, :, 0, 2][_mask_data[:, :, 0]]
        # _s2 = _s[:][:, :, 1, 2][_mask_data[:, :, 1]]
        return _s1
