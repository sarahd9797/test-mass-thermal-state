import numpy as np
import pandas as pd
import h5py
import tensorflow as tf


full_params = pd.read_csv(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/training/training_params.csv"
)

random_choice = np.unique(np.random.uniform(low=0, high=len(full_params), size=5000))
subset = full_params[0:5000]


# check accuracy against training data
rombus_model = h5py.File(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/"
    + "batch_runs/model/deformation_rom/model_HR_from_h5.hdf5",
    "r",
)
ei_nodes = rombus_model.get("upsampled_interpolant/nodes")[:]
training_set = h5py.File(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/combined_TS.h5",
    "r",
)

_training_data = np.ravel(
    training_set.get("7346/deformation/deformation_data")[:][:, :, 1, 2]
)


_training_data = _training_data[ei_nodes]


N_batch = len(subset)
defo = np.zeros((N_batch, len(_training_data)))
params = np.zeros((N_batch, 3))

for _ii in range(N_batch):
    defo[_ii] = np.ravel(
        training_set.get("%i/deformation/deformation_data" % _ii)[:][:, :, 1, 2]
    )[ei_nodes]
    params[_ii] = np.array([subset["dx"][_ii], subset["dy"][_ii], subset["w"][_ii]])
print(np.min(defo))
defo /= np.min(defo)


def generate_ml_model(in_params, out_timeseries):
    model = tf.keras.Sequential(
        [
            tf.keras.layers.Dense(in_params.shape[0], activation="relu"),
            tf.keras.layers.Dense(32, activation="relu"),
            tf.keras.layers.Dense(64, activation="relu"),
            tf.keras.layers.Dense(128, activation="relu"),
            tf.keras.layers.Dense(out_timeseries.shape[1]),
        ]
    )
    model.compile(
        optimizer=tf.keras.optimizers.Adam(0.001), loss="mse", metrics=["mae", "mse"]
    )
    model.fit(in_params.T, out_timeseries, epochs=10000, verbose=1)
    return model


if __name__ == "__main__":
    ml_model = generate_ml_model(params.T, defo)
    ml_model.save("tf_HR_defo")
