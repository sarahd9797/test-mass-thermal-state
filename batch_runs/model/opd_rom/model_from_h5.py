from numpy import ndarray, polyval, linspace
from rombus.model import RombusModel
from typing import NamedTuple
import pandas as pd
import numpy as np
import h5py 

_h = h5py.File('/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/combined_TS.h5', 'r')
_t = _h.get('4/opd/OPD')
_tmask = np.logical_not(np.isnan(_t[:])) 
_tnonan = _t[_tmask]

class Model(RombusModel):
    ##NOTE: these are masked, raveled (1D) coordinates
    coordinate.set("x", 0., float(len(_tnonan)-1), len(_tnonan), label="$x$")

    ordinate.set("y", label="$y(x)$")

    params.add("idx", 0, 10000)

    def compute(self, p: NamedTuple, x: ndarray) -> ndarray:
        """Compute the model for a given parameter set."""
        _m = _h.get('%i/opd/OPD'%(p.idx))
        _mmask = np.logical_not(np.isnan(_m[:]))
        return _m[_mmask] 
