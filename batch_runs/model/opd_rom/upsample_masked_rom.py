import h5py
import numpy as np

_h = h5py.File('/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/combined_TS.h5', 'r')
_t = _h.get('4/opd/OPD')
_tmask = np.logical_not(np.isnan(_t[:]))
_tnonan = _t[_tmask]
_ravel_mask = np.ravel(_tmask)

idxx = [np.where(np.ravel(_t[:]) == a) for a in _tnonan]
map_idx = np.array(idxx).flatten()

rombus_model_out = h5py.File('/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/rom/model_from_h5.hdf5', 'r+')

ei_nodes = rombus_model_out.get('empirical_interpolant/nodes')[:].astype(int)

ei_map_idx = map_idx[ei_nodes]

B_ravel = rombus_model_out.get('empirical_interpolant/B_matrix')[:]

B_full_size = np.zeros((B_ravel.shape[0], _t[:].shape[0]*_t[:].shape[1]))
B_full_size.fill(np.nan)

for _ii, _B in enumerate(B_full_size):

    _b = np.ravel(_B)
    _b[map_idx] = B_ravel[_ii]
    B_full_size[_ii] = _b.reshape(_t[:].shape[0]* _t[:].shape[1])

grp = rombus_model_out.create_group('upsampled_interpolant')
dset_B = grp.create_dataset('B_matrix', data=B_full_size)
dset_ei_nodes = grp.create_dataset('nodes', data=ei_map_idx)

