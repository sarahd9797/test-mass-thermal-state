import h5py
import numpy as np
import pylab as plt

rombus_model = h5py.File('/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/rom/model_from_h5.hdf5', 'r')

B_matrix = rombus_model.get('upsampled_interpolant/B_matrix')[:]

ei_nodes = rombus_model.get('upsampled_interpolant/nodes')[:]

#check accuracy against training data
training_set = h5py.File('/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/combined_TS.h5','r')

training_data = training_set.get('4620/opd/OPD')[:]

_ravel_training_data = np.ravel(training_data)

data_at_nodes = _ravel_training_data[ei_nodes]

_ravel_rom = np.dot(data_at_nodes, B_matrix)

rom = _ravel_rom.reshape(training_data.shape[0], training_data.shape[1])


x, y, z = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.1, 0.1, 5),
)
# %%
plt.contourf(
    x,
    y,
    rom / 1e-6,
    20,
)
plt.gca().set_aspect("equal")
cb0 = plt.colorbar()
cb0.ax.set_ylabel("Optical path depth [um]")
plt.xlabel("x [m]")
plt.ylabel("y [m]")
plt.title("ROM")
plt.savefig("OPD_rom.pdf", bbox_inches='tight')
plt.clf()

plt.contourf(
    x,
    y,
    training_data / 1e-6,
    20,
)
plt.gca().set_aspect("equal")
cb0 = plt.colorbar()
cb0.ax.set_ylabel("Optical path depth [um]")
plt.xlabel("x [m]")
plt.ylabel("y [m]")
plt.title("FOM")
plt.savefig("OPD_FOM.pdf", bbox_inches='tight')
plt.clf()

