import tensorflow as tf
import numpy as np
import pandas as pd
import h5py
import pylab as plt
import sys

new_model = tf.keras.models.load_model("./tf_opd.h5")

full_params = pd.read_csv(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/training/training_params.csv"
)

random_choice = np.unique(np.random.uniform(low=0, high=len(full_params), size=5000))


# check accuracy against training data
rombus_model = h5py.File(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/opd_rom/model_from_h5.hdf5",
    "r",
)
ei_nodes = rombus_model.get("upsampled_interpolant/nodes")[:]
B_matrix = rombus_model.get("upsampled_interpolant/B_matrix")[:]


training_set = h5py.File(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/combined_TS.h5",
    "r",
)

_training_data = np.ravel(training_set.get("4620/opd/OPD")[:])[ei_nodes]

for i in range(5800, 5900):
    param = np.array([full_params["dx"][i], full_params["dy"][i], full_params["w"][i]])
    true_opd = np.ravel(training_set.get("%i/opd/OPD" % i)[:])[ei_nodes]
    FOM = np.dot(true_opd, B_matrix).reshape(200, 200)
    fit_opd = new_model.predict(np.array([param]))[0] * 4.308446821664332e-07
    ROM = np.dot(fit_opd, B_matrix).reshape(200, 200)

    x, y, z = (
        np.linspace(-0.17, 0.17, 200),
        np.linspace(-0.17, 0.17, 200),
        np.linspace(-0.1, 0.1, 5),
    )
    # %%
    plt.contourf(
        x,
        y,
        ROM / 1e-6,
        20,
    )
    plt.gca().set_aspect("equal")
    cb0 = plt.colorbar()
    cb0.ax.set_ylabel("Optical path depth [um]")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.title("ROM")
    plt.savefig("OPD_mlrom.pdf", bbox_inches="tight")
    plt.clf()

    plt.contourf(
        x,
        y,
        FOM / 1e-6,
        20,
    )
    plt.gca().set_aspect("equal")
    cb0 = plt.colorbar()
    cb0.ax.set_ylabel("Optical path depth [um]")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.title("FOM")
    plt.savefig("OPD_fom.pdf", bbox_inches="tight")
    plt.clf()

    plt.contourf(
        x,
        y,
        (np.abs((ROM - FOM) / FOM)) * 100,
        20,
    )
    plt.gca().set_aspect("equal")
    cb0 = plt.colorbar()
    cb0.ax.set_ylabel("percentage error")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.title("relative_error")
    plt.savefig("relative_error_rom.pdf", bbox_inches="tight")
    plt.clf()

    sys.exit()
