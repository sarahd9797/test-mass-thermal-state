# %%


from ifo_thermal_state.aligo_3D import (
    make_test_mass_model,
    AdvancedLIGOTestMass3DSteadyState,
)
from ifo_thermal_state.math import composite_newton_cotes_weights
import matplotlib.pyplot as plt
import numpy as np
from types import SimpleNamespace

import pandas as pd
import h5py
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-p", "--paramfile", dest="paramfile")
parser.add_option("-b", "--batch", dest="batch", type=int)

(options, args) = parser.parse_args()

stride = 10

training_params = pd.read_csv('%s'%(options.paramfile))

slice_1 = int(options.batch)*stride
slice_2 = min(int(options.batch+1)*stride, len(training_params))
training_params = pd.read_csv('%s'%(options.paramfile))[slice_1:slice_2]

# %%
model = make_test_mass_model(mesh_function_kwargs={
    "HR_mesh_size":0.02, "AR_mesh_size":0.02, "mesh_algorithm": 0
})

print(training_params)
hf = h5py.File('/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/'+'TS_%d.h5'%(options.batch), 'w')

for _ii, idx in enumerate(training_params['idx']):
    group_name = f'{idx}'
    group = hf.create_group(group_name)

    values = SimpleNamespace()
    values.w = training_params['w'][idx] 
    values.dx = training_params['dx'][idx]
    values.dy = training_params['dy'][idx]
    values.P_coat = 1
   
    
    print(values.w, values.dx, values.dy)
    def I_HR(x):
        I0 = 2 * values.P_coat / (np.pi * values.w**2)
        return I0 * np.exp(
            -2 * ((x[0] - values.dx) ** 2 + (x[1] - values.dy) ** 2) / (values.w**2)
        )
    
    
    ss = AdvancedLIGOTestMass3DSteadyState(model)
    
    # %%
    ss.temperature.I_HR.interpolate(I_HR)
    ss.solve_temperature()
    ss.solve_deformation()
    
    # %%
    x, y, z = (
        np.linspace(-0.17, 0.17, 200),
        np.linspace(-0.17, 0.17, 200),
        [-0.1, 0.1],
    )
    deformation_group = group.create_group('deformation')
    x_data = deformation_group.create_dataset('x_data', data=x)
    y_data = deformation_group.create_dataset('y_data', data=y)
    z_data = deformation_group.create_dataset('z_data', data=z)
    # %%
    ###******###
    ###save S###
    xyz, S, mask = ss.evaluate_deformation(x, y, z, meshgrid=True)
    def_data = deformation_group.create_dataset('deformation_data', data=S)
    mask_data = deformation_group.create_dataset('mask_data', data=mask)
    opd_group = group.create_group('opd')
    # %% 3D integration
    x, y, z = (
        np.linspace(-0.17, 0.17, 200),
        np.linspace(-0.17, 0.17, 200),
        np.linspace(-0.1, 0.1, 5),
    )

    # %%
    x_data_opd = opd_group.create_dataset('x_data', data=x)
    y_data_opd = opd_group.create_dataset('y_data', data=y)
    z_data_opd = opd_group.create_dataset('z_data', data=z)
    xyz, dT, mask = ss.evaluate_temperature(x, y, z, meshgrid=True)
    # %%
    # Use better quadrature rule for integratiopn
    weights = composite_newton_cotes_weights(z.size, 5)
    dz = z[1] - z[0]
    ###******###
    ### Save OPD ###
    OPD = (
        model.dndT
        * dz
        * np.sum(dT[:, :, :, 0] * weights[None, None, :], axis=2)  # weight Z direction and sum
    )
    opd_data = opd_group.create_dataset('OPD', data=OPD)
hf.close()
