import numpy as np
import pandas as pd 


dx_min, dx_max = -40e-3, 40e-3
dy_min, dy_max = -40e-3, 40e-3
w_min, w_max =  45e-3, 70e-3 

# sample boundary
def sample_boundary(N):
    dx = np.random.choice([dx_min, dx_max],size=N)
    dy = np.random.choice([dy_min, dy_max],size=N) 
    w = np.random.choice([w_min, w_max], size=N)
    return np.array([dx, dy, w]).T

#sample interior 
def sample_interior(M):

    dx = np.random.uniform(low=dx_min, high=dx_max,size=M)
    dy = np.random.uniform(low=dy_min, high=dy_max, size=M)
    w = np.random.uniform(low=w_min, high=w_max, size=M)
    return np.array([dx, dy, w]).T

a = sample_boundary(100)
b = sample_interior(10000)

ts = np.vstack((a,b))

colnames=['dx', 'dy', 'w'] 

ts_df = pd.DataFrame(ts, columns=colnames)
ts_df.to_csv('training_params.csv')

