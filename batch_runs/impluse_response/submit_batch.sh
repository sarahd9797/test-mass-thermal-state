#!/bin/bash
#SBATCH --job-name=map_rom_training_data   # Job name
#SBATCH --output=/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/impluse_response/log/array_%A-%a.log    # Standard output and error log
#SBATCH --array=0-1500                # Array range
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:120:00
#SBATCH --mem-per-cpu=5000
#SBATCH --account=oz170
. /home/rsmith/.bashrc; module load conda; conda activate fenicsx-env

python /fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/impluse_response/impulse_response_AB.py -i $SLURM_ARRAY_TASK_ID -p /fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/impluse_response/training_params.csv
