import numpy as np
import pandas as pd


dx_min, dx_max = -20e-3, 20e-3
dy_min, dy_max = -20e-3, 20e-3
w_min, w_max = 45e-3, 70e-3


# sample boundary
def sample_boundary(N):
    dx = np.zeros(N)  # np.random.choice([dx_min, dx_max], size=N)
    dy = np.zeros(N)  # np.random.choice([dy_min, dy_max], size=N)
    w = np.random.choice([w_min, w_max], size=N)
    return np.array([w, dx, dy]).T


# sample interior
def sample_interior(M):
    dx = np.zeros(M)  # np.random.uniform(low=dx_min, high=dx_max, size=M)
    dy = np.zeros(M)  # np.random.uniform(low=dy_min, high=dy_max, size=M)
    w = np.random.uniform(low=w_min, high=w_max, size=M)
    return np.array([w, dx, dy]).T


a = sample_boundary(500)
b = sample_interior(1500)

ts = np.vstack((a, b))
ts = np.unique(ts, axis=0)
colnames = ["w", "dx", "dy"]  # ['dx', 'dy', 'w']

ts_df = pd.DataFrame(ts, columns=colnames)
ts_df.to_csv("training_params.csv")
