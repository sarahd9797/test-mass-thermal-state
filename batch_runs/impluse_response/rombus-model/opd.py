from numpy import ndarray
from rombus.model import RombusModel
from typing import NamedTuple
import numpy as np


class Model(RombusModel):
    # NOTE: these are masked, raveled (1D) coordinates
    coordinate.set(
        "x",
        0.0,
        float(127 * 100 * 100 - 1),
        int(127 * 100 * 100),
        label="$x$",
    )

    ordinate.set("y", label="$y(x)$")

    params.add("idx", 0, 1499)

    def compute(self, p: NamedTuple, x: ndarray) -> ndarray:
        """Compute the model for a given parameter set."""
        _s = np.load(
            "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/impluse_response/outdir/OPD_HRz_%i.npz"
            % (p.idx)
        )['OPD_nm']
        _s_ravel = _s.ravel()
        return _s_ravel
