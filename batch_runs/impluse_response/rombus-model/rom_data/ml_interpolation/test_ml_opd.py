import numpy as np
import pandas as pd
import tensorflow as tf

b = np.load("../B_matrix_opd.npy")

new_model = tf.keras.models.load_model("./tf_opd.h5")

full_params = pd.read_csv(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/impluse_response/training_params.csv"
)
scalar = np.load("mean_and_var.npy", allow_pickle=True)
mean = scalar.item()["mean"]
std = scalar.item()["std"]

for _ii in range(1000):
    params = np.zeros(2)
    _data = np.load("../../../outdir/OPD_HRz_%i.npz" % _ii)['OPD_nm']
    params[0] = full_params["wx"][_ii]
    params[1] = full_params["wy"][_ii]
    ei_coeffs = (new_model.predict(np.array([params]))[0] + mean) * std
    rom = np.einsum("ijkl, i->jkl", b, ei_coeffs)
    print(rom, _data)
    err = np.abs((rom - _data) / _data)
    print(np.max(err[~np.isnan(err)]))
