import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras import layers

b = np.load("../../../B_matrix_opd.npy")
_xyt_grid = np.load("../../../../outdir/OPD_HRz_10.npz")
x = _xyt_grid['x']
y = _xyt_grid['y']
t = _xyt_grid['t']

grid_1, grid_2, grid_3 = np.meshgrid(t,x,y, indexing='ij')

# Stack the coordinate grids along the last axis to create a 3D array
coordinates_3d = np.stack((grid_1, grid_2, grid_3), axis=-1)

coordinates_3d_ravelled = np.ravel(coordinates_3d).reshape(1270000,3)

params = coordinates_3d_ravelled
training_set = np.ravel(b[0])
training_set = training_set.reshape(len(training_set),1)

from sklearn.model_selection import train_test_split

X_train, X_test, Y_train, Y_test = train_test_split(params, training_set, test_size=0.2)


def generate_ml_model(params, training_set):
    model = tf.keras.Sequential()
    model.add(
        layers.Input(shape=(params.shape[1],))
    )  # Input layer corresponding to the 3 features
    model.add(layers.Dense(8, activation="relu"))
    model.add(layers.Dense(256, activation="relu"))  # Fully connected layer
    model.add(layers.Dense(512, activation="relu"))  # Another fully connected layer
    model.add(layers.Dense(1024, activation="relu"))  # Another fully connected layer
    model.add(layers.Dense(512, activation="relu"))  # Another fully connected layer
    model.add(layers.Dense(256, activation="relu"))  # Fully connected layer
    model.add(layers.Dense(128, activation="relu"))
    model.add(
        layers.Dense(training_set.shape[1])
    )  # Output layer corresponding to the 1-dimensional output

    # Compile the model
    model.compile(
        loss="mse",  # mean squared error, standard for regression problems
        optimizer="adam",  # Adam optimization is a stochastic gradient descent method
        metrics=["mae"],
    )  # mean absolute error, another common metric for regression
    return model


if __name__ == "__main__":
    ml_model = generate_ml_model(params, training_set)
    history = ml_model.fit(
        X_train,
        Y_train,
        epochs=50,  # The number of iterations over the entire dataset
        # batch_size=400,  # Number of samples per gradient update
        validation_data=(X_test, Y_test),
    )  # We evaluate the model's performance on the test set after each epoch

    ml_model.save("tf_b_0.h5")
