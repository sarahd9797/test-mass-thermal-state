import numpy as np
import pandas as pd
import tensorflow as tf
import sys
from tensorflow.keras import layers

TS_size = 1300
full_params = pd.read_csv(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/impluse_response/training_params.csv"
)
nodes = np.load("../nodes_opd_txy.npy")
opd_training_set = np.zeros((TS_size, len(nodes)))

params = np.zeros((TS_size, 3))

for _ii in range(TS_size):
    _data = np.load("../../../outdir/OPD_nm_%i.npy" % _ii)
    opd_training_set[_ii] = np.array(
        [
            _data[nodes[_jj][0], nodes[_jj][1], nodes[_jj][2]]
            for _jj in range(len(nodes))
        ]
    )
    params[_ii][0] = full_params["w"][_ii]
    params[_ii][1] = full_params["dx"][_ii]
    params[_ii][2] = full_params["dy"][_ii]

np.save("opd_training_set", opd_training_set)

from sklearn.model_selection import train_test_split

X_train, X_test, Y_train, Y_test = train_test_split(
    params, opd_training_set, test_size=0.2
)
"""
def generate_ml_model(in_params, out_data):
    model = tf.keras.Sequential(
        [
            #tf.keras.layers.Input(shape=(in_params.shape[1],)),
            tf.keras.layers.Dense(in_params.shape[1], activation="relu"),
            tf.keras.layers.Dense(128, activation="relu"),
            tf.keras.layers.Dense(64, activation="relu"),
            tf.keras.layers.Dense(out_data.shape[1]),
        ]
    )
    model.compile(
        optimizer='adam', loss="mse", metrics=["mae", "mse"]
    )
    model.fit(in_params, out_data, epochs=1000, verbose=1)
    return model
"""


def generate_ml_model(params, opd_training_set):
    model = tf.keras.Sequential()
    model.add(
        layers.Input(shape=(params.shape[1],))
    )  # Input layer corresponding to the 3 features
    model.add(layers.Dense(16, activation="relu"))  # Fully connected layer
    model.add(layers.Dense(32, activation="relu"))  # Another fully connected layer
    model.add(layers.Dense(64, activation="relu"))
    model.add(layers.Dense(128, activation="relu"))
    # model.add(layers.Dense(256, activation='relu'))
    model.add(
        layers.Dense(opd_training_set.shape[1])
    )  # Output layer corresponding to the 36-dimensional output

    # Compile the model
    model.compile(
        loss="mse",  # mean squared error, standard for regression problems
        optimizer="adam",  # Adam optimization is a stochastic gradient descent method
        metrics=["mae"],
    )  # mean absolute error, another common metric for regression
    return model


if __name__ == "__main__":
    ml_model = generate_ml_model(params, opd_training_set)
    history = ml_model.fit(
        X_train,
        Y_train,
        epochs=1000,  # The number of iterations over the entire dataset
        # batch_size=400,  # Number of samples per gradient update
        validation_data=(X_test, Y_test),
    )  # We evaluate the model's performance on the test set after each epoch

    ml_model.save("tf_opd.h5")
