import numpy as np
import pandas as pd
import h5py
import tensorflow as tf
import sys
import joblib
import time

full_params = pd.read_csv(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/impluse_response/training_params.csv"
)
nodes = np.load("../nodes_opd_txy.npy")
b = np.load("../B_matrix_opd.npy")

opd_training_set = np.zeros(len(nodes))

params = np.zeros((3))

# scaler_X = joblib.load("../ml_interpolation/scaler_X_opd")

# If your Y data benefits from scaling, do it similarly
# scaler_Y = joblib.load("../ml_interpolation/scaler_Y_opd")
opd_predictor = tf.keras.models.load_model("../ml_interpolation/tf_opd.h5")

for _ii in range(1000, 1010):
    params[0] = full_params["w"][_ii]
    params[1] = full_params["dx"][_ii]
    params[2] = full_params["dy"][_ii]

    t1 = time.time()
    for i in range(100):
        ei_coeffs = opd_predictor.predict(np.array([params]))[0]
        opd_ROM = np.einsum("ijkl, i->jkl", b, ei_coeffs)
    t2 = time.time()
    print((t2 - t1) / 100.0)
