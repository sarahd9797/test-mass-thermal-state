import numpy as np
import pandas as pd
import tensorflow as tf

param_ranges = pd.read_csv("param_ranges.csv")
opd_predictor = tf.keras.models.load_model("../ml_interpolation/tf_defo.h5")

nodes = np.load("../nodes_surface_deformation_txy.npy")
b = np.load("../B_matrix_surface_deformation.npy")


def check_ranges(w, dx, dy):
    if (
        w > param_ranges["w_max"]
        or w < param_ranges["w_min"]
        or dx > param_ranges["dx_max"]
        or dx < param_ranges["dx_min"]
        or dy > param_ranges["dy_max"]
        or dy < param_ranges["dy_min"]
    ):
        return 0
    else:
        return 1


def opd_ROM(w, dx, dy):
    params = np.zeros(3)

    if check_ranges(w, dx, dy):
        params[0] = w
        params[1] = dx
        params[2] = dy

        ei_coeffs = opd_predictor.predict(np.array([params]))[0]
        opd_ROM = np.einsum("ijkl, i->jkl", b, ei_coeffs)

        return opd_ROM
    else:
        print("parameters are out of range. Check param_ranges.csv")
        return 0
