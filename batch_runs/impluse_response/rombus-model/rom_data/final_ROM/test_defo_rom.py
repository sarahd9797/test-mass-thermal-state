import numpy as np
import pandas as pd
import h5py
import tensorflow as tf
import sys
import joblib

full_params = pd.read_csv(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/impluse_response/training_params.csv"
)
nodes = np.load("../nodes_surface_deformation_txy.npy")
b = np.load("../B_matrix_surface_deformation.npy")

surface_deformation_training_set = np.zeros(len(nodes))

params = np.zeros((3))

# scaler_X = joblib.load("../ml_interpolation/scaler_X_surface_deformation")

# If your Y data benefits from scaling, do it similarly
# scaler_Y = joblib.load("../ml_interpolation/scaler_Y_surface_deformation")
surface_deformation_predictor = tf.keras.models.load_model(
    "../ml_interpolation/tf_defo.h5"
)

for _ii in range(1000, 1010):
    _data = np.load("../../../outdir/z_HR_nm_%i.npy" % _ii)
    surface_deformation_training_set = np.array(
        [
            _data[nodes[_jj][0], nodes[_jj][1], nodes[_jj][2]]
            for _jj in range(len(nodes))
        ]
    )
    params[0] = full_params["w"][_ii]
    params[1] = full_params["dx"][_ii]
    params[2] = full_params["dy"][_ii]

    # _scaled_params_for_ml = scaler_X.fit_transform([params])
    # ei_coeffs_scaled = surface_deformation_predictor.predict([_scaled_params_for_ml])[0]
    # ei_coeffs = scaler_Y.inverse_transform([ei_coeffs_scaled])[0]
    ei_coeffs = surface_deformation_predictor.predict(np.array([params]))[0]
    surface_deformation_ROM = np.einsum("ijkl, i->jkl", b, ei_coeffs)
    surface_deformation_FOM = np.einsum(
        "ijkl, i->jkl", b, surface_deformation_training_set
    )

    rom_norm = (
        np.dot(surface_deformation_ROM.ravel(), surface_deformation_ROM.ravel()) ** 0.5
    )
    fom_norm = (
        np.dot(surface_deformation_FOM.ravel(), surface_deformation_FOM.ravel()) ** 0.5
    )
    np.save("ROM_%d" % _ii, surface_deformation_ROM)
    np.save("FOM_%d" % _ii, surface_deformation_FOM)
    print(
        np.dot(surface_deformation_ROM.ravel(), surface_deformation_FOM.ravel())
        / rom_norm
        / fom_norm
    )
