import numpy as np
import pylab as plt

import time

b = np.load("B_matrix_opd.npy")
n = np.load("nodes_opd_txy.npy")


fom = np.load("../../outdir/OPD_nm_1100.npy")
ei_coeffs = np.array([fom[n[i][0], n[i][1], n[i][2]] for i in range(len(n))])
rom = np.einsum("ijkl, i->jkl", b, ei_coeffs)
"""t1 = time.time()
for _ii in range(1000):
    rom = np.einsum('ijkl, i->jkl', b, ei_coeffs)
t2 = time.time()

print((t2-t1)/1000.)"""

from celluloid import Camera

# Create figure and axis objects
fig, ax = plt.subplots()

# Set up the camera from celluloid module
camera = Camera(fig)

for i in range(fom.shape[0]):
    frame = fom[i, :, :]
    ax.imshow(
        frame, cmap="viridis", animated=True
    )  # You can choose different colormaps suitable for your data
    # ax.set_title(f"Frame {i + 1}")  # Optional title to display frame number
    plt.axis("off")  # Optional: turn off the axis for each frame
    # plt.colorbar(title='OPD (nm)')
    camera.snap()  # Take a snapshot after the image is plotted
plt.title("FOM")
# Create the animation
animation = camera.animate(
    blit=True, interval=50
)  # 'interval' controls the speed of the animation

# Save as GIF
animation.save(
    "FOM.gif", writer="imagemagick"
)  # You may need to install ImageMagick for GIF saving

plt.clf()

# Create figure and axis objects
fig, ax = plt.subplots()

# Set up the camera from celluloid module
camera = Camera(fig)

for i in range(rom.shape[0]):
    frame = rom[i, :, :]
    ax.imshow(
        frame, cmap="viridis", animated=True
    )  # You can choose different colormaps suitable for your data
    # ax.set_title(f"Frame {i + 1}")  # Optional title to display frame number
    plt.axis("off")  # Optional: turn off the axis for each frame
    camera.snap()  # Take a snapshot after the image is plotted
plt.title("ROM")
# Create the animation
animation = camera.animate(
    blit=True, interval=50
)  # 'interval' controls the speed of the animation

# Save as GIF
animation.save(
    "ROM.gif", writer="imagemagick"
)  # You may need to install ImageMagick for GIF saving

plt.clf()

errors = np.abs((rom.ravel() - fom.ravel()) / fom.ravel())

plt.hist(errors[~np.isnan(errors)], bins=15)
plt.xlabel("fractional error: abs((rom-fom)/fom))")

plt.savefig("error_histogram.pdf", bbox_inches="tight")
