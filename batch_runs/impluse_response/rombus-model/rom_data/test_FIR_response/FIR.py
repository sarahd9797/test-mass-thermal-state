import numpy as np
import tensorflow as tf
import time

opd_predictor = tf.keras.models.load_model("../ml_interpolation/tf_opd.h5")
b = np.load("../B_matrix_opd.npy")


def opd(t, T, pulse_params):
    OPD = 0
    # for theta in pulse_params:
    if t < T:
        # for param in pulse_params:
        ei_coeffs = opd_predictor.predict(pulse_params)
        # ei_coeffs = opd_predictor.predict(tf.stack([pulse_params[i] for i in range(len(pulse_params))]))
        b_at_t = b_at_t = b[:, t, :, :]
        opd_ROM = np.einsum("ijk, li->jk", b_at_t, ei_coeffs)
        # OPD += opd_ROM
        # opd_ROM = np.einsum("ijk, il->jkl", b_at_t, ei_coeffs)
    return opd_ROM


t1 = time.time()

pulses = []  # pulses that have happend at each time step
OPDs = []  # array of OPDs at time step
t0 = 0
N_steps = 100
dt = 1
T = int(N_steps * dt)
for i in range(N_steps):
    t = int(t0 + i * dt)
    w = np.random.uniform(low=45e-3, high=70e-3)
    dx = np.random.uniform(low=-20e-3, high=20e-3)
    dy = np.random.uniform(low=-20e-3, high=20e-3)
    params = [w, dx, dy]
    pulses.append(params)
    OPDs.append(opd(t, T, pulses))
t2 = time.time()
print(t2 - t1)

import finesse
from finesse.knm.maps import Map
t3 = time.time()
model = finesse.Model()
model.modes(maxtem=14)
model.modes()
x = y = np.linspace(-1, 1, 100)
X, Y = np.meshgrid(x, y)
m = Map(x, y, amplitude=np.zeros_like(X), opd=np.array(OPDs[0]))
q = finesse.BeamParam(w0=0.3, z=0)
k0 = 2 * np.pi / 1064e-9
Knm = m.scatter_matrix(q, k0, 2, model.homs)

t4 = time.time()

print(t4 - t3)
