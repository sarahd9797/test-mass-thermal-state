import numpy as np
import h5py

rombus_model_out = h5py.File(
    "/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/impluse_response/rombus-model/opd.hdf5",
    "r",
)


ei_nodes = rombus_model_out.get("empirical_interpolant/nodes")[:].astype(int)

B_ravel = rombus_model_out.get("empirical_interpolant/B_matrix")[:]

B_full_size = np.zeros((B_ravel.shape[0], 127, 100, 100))

for _ii, _B in enumerate(B_full_size):
    _b = B_ravel[_ii]
    B_full_size[_ii] = _b.reshape(127, 100, 100)

nodes_unravelled = np.unravel_index(ei_nodes, (127, 100, 100))
np.save("B_matrix_opd.npy", B_full_size)
np.save("nodes_opd_txy.npy", np.array(nodes_unravelled).T)
