#!/bin/bash
#SBATCH --job-name=map_rom_training_data   # Job name
#SBATCH --output=/fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/slurm/log/array_%A-%a.log    # Standard output and error log
#SBATCH --array=0-1001                 # Array range
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:5:00        
#SBATCH --mem-per-cpu=10000
#SBATCH --account=oz170
. /home/rsmith/.bashrc; module load conda; conda activate tcs 

python /fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/model/aligo.py -b $SLURM_ARRAY_TASK_ID -p /fred/oz170/rsmith/git/test-mass-thermal-state/batch_runs/training/training_params.csv 

