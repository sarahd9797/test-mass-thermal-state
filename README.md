# Thermal state modelling of optical elements

A package for modelling the thermal state of test mass and other optics due to thermal actuations and laser beam heating.

This package relies on the finite element modelling tool FEniCSX[https://fenicsproject.org/].

Notes are stored: https://docs.google.com/document/d/1tacDguDpGkRwdsqtgt_AKRb5-UzlT-4BwrWbPjlf92U/edit

## Tips

When using FEniCSx from Conda-Forge on MacOS an error appears when trying to compile UFI/ffcx expressions that `x86_64-apple-darwin13.4.0-clang` could not be found. This was fixed by installing `conda install clangxx_osx-64` which doesn't seem to be included in the conda package for FEniCSx.

## Contributing

If you are developing this code install pre-commit and enable it to standardise the Python code.
```
cd [git repo]
pip install pre-commit
pre-commit install
```

## Authors and acknowledgment

Huy Tuong Cao
Daniel Brown
Rory Smith

## License
GPL v3
