from ifo_thermal_state.math import composite_newton_cotes_weights
import numpy as np


def get_mask(x, y, ss, meshgrid=True):
    """For a given solution object return an array of x,y points which are beyond the
    mesh.

    Parameters
    ----------
    x, y : arraylike
        If meshgrid is True, these should be 1D vectors describing the
        volume of points to evaluate the solution over. These inputs
        will be inputted to meshgrid to determine what points to evaluate.
        If meshgrid is False then x,y,z describe the array of points to
        evaluate at. If False x,y,z must be the same length or singular
        values.
    ss : [AdvancedLIGOTestMass3DSteadyState | AdvancedLIGOTestMass3DTime]
        Should be a solution, either a steady state or time domain to extract
        the result  from.
    meshgrid : bool, optional
        Whether to use meshgrid to project the provided x,y,z values
        to a grid on which to evaluate

    Returns
    -------
    array[dtype=int, ndim=2]
        2D binary mask, 1 inside mesh, 0 outside.
    """
    _, _, mask = ss.evaluate_deformation(x, y, 0.1, meshgrid=meshgrid)
    return mask.astype(int)[:, :, 0]


def get_deformation(x, y, ss, h=0.1, meshgrid=True):
    """For a given solution extract the HR surface deformation.

    Parameters
    ----------
    x, y : arraylike
        If meshgrid is True, these should be 1D vectors describing the
        volume of points to evaluate the solution over. These inputs
        will be inputted to meshgrid to determine what points to evaluate.
        If meshgrid is False then x,y,z describe the array of points to
        evaluate at. If False x,y,z must be the same length or singular
        values.
    ss : [AdvancedLIGOTestMass3DSteadyState | AdvancedLIGOTestMass3DTime]
        Should be a solution, either a steady state or time domain to extract
        the result  from.
    h : float, optional
        Surface z position to extract, aLIGO is 20cm thick and HR surface in the
        included model is set at +10cm. AR is at -10cm.
    meshgrid : bool, optional
        Whether to use meshgrid to project the provided x,y,z values
        to a grid on which to evaluate

    Returns
    -------
    array[dtype=int, ndim=2]
        2D binary mask, 1 inside mesh, 0 outside.
    """
    _, S, mask = ss.evaluate_deformation(x, y, h, meshgrid=meshgrid)
    S[~mask] = 0
    # Selects the z motion
    return S[:, :, 0, 2]


def get_opd(x, y, ss, h=0.1, N=11, newton_cotes_order=5, meshgrid=True, dndT=8.6e-06):
    """For a given solution integrate through the optic to extract the optical path
    depth from a temperature change through the substrate.

    Parameters
    ----------
    x, y : arraylike
        If meshgrid is True, these should be 1D vectors describing the
        volume of points to evaluate the solution over. These inputs
        will be inputted to meshgrid to determine what points to evaluate.
        If meshgrid is False then x,y,z describe the array of points to
        evaluate at. If False x,y,z must be the same length or singular
        values.
    ss : [AdvancedLIGOTestMass3DSteadyState | AdvancedLIGOTestMass3DTime]
        Should be a solution, either a steady state or time domain to extract
        the result  from.
    h : float, optional
        half-thickness of optic, aLIGO test mass is 20cm thick, so h=0.1
        Integrates from -h to h.
    N : int, optional
        Number of z samples to take for integration
    newton_cotes_order : int, optional
        Order of the newton-cotes integration kernel to use
    meshgrid : bool, optional
        Whether to use meshgrid to project the provided x,y,z values
        to a grid on which to evaluate
    dndT : float
        Defaults to fused silica for LIGO optics

    Returns
    -------
    array[dtype=float, ndim=2]
        2D optical path depth, units of metres
    """
    z = np.linspace(-h, h, N)
    _, dT, mask = ss.evaluate_temperature(x, y, z, meshgrid=meshgrid)
    dT[~mask] = 0
    # Use better quadrature rule for integratiopn
    weights = composite_newton_cotes_weights(z.size, newton_cotes_order)
    dz = z[1] - z[0]
    OPD = (
        dndT
        * dz
        * np.sum(
            dT[:, :, :, 0] * weights[None, None, :], axis=2
        )  # weight Z direction and sum
    )
    return OPD
