import gmsh
from dolfinx.io import gmshio
from mpi4py import MPI
from scipy.spatial.distance import cdist


def cylinder_3D(
    radius,
    thickness,
    add_flats=False,
    flat_hw=0.3265 / 2,
    HR_mesh_size=0.02,
    AR_mesh_size=0.02,
    point=None,
    *,
    mesh_algorithm=0,
    name="cyclinder",
    save_filename=None,
):
    """Creates a 3-dimensional cylinder with a HR (z=+thickness) and AR (z=-thickness)
    surface.

    Parameters
    ----------
    radius, thickness : float
        Radius and thickness of cylinder in meters
    add_flats: boolean
        Add flat faces to the sides of test mass
    flat_hw: float
        test mass halfwidth at center where the side flats are
        (only apply if add_flats is set to True)
        Default: 0.16325
    HR_mesh_size, AR_mesh_size : float
        Sets target mesh size at each surface in units of meters
    name : str, optional
        Name of cylinder in the geometry
    save_filename : str
        Name and path of file to save geometry too if needed, if None not saved.

    Returns
    -------
    model : gmsh.model()
    cell_markers : dolfinx.cpp.mesh.MeshTags_int32
    facet_markers : dolfinx.mesh.MeshTagsMetaClass
    """
    gmsh.initialize()
    # Choose if Gmsh output is verbose
    gmsh.option.setNumber("General.Terminal", 0)
    model = gmsh.model()

    model.add(name)
    model.setCurrent(name)
    cyl = model.occ.addCylinder(0, 0, -thickness / 2, 0, 0, thickness, radius, tag=1)

    if add_flats is True:
        dBox = radius - flat_hw

        # Add boxes on the sides to subtract:
        model.occ.addBox(
            -radius - dBox,
            -radius,
            -thickness / 2,
            dBox * 2,
            2 * radius,
            thickness,
            tag=2,
        )
        model.occ.addBox(
            radius - dBox,
            -radius,
            -thickness / 2,
            dBox * 2,
            2 * radius,
            thickness,
            tag=3,
        )

        cut_cyl = model.occ.cut([(3, 1)], [(3, 2), (3, 3)], 4)
        cyl = cut_cyl[0][0][1]

    # if point is not None:
    #     point_tag = gmsh.model.occ.addPoint(*point, thickness/2)

    # Synchronize OpenCascade representation with gmsh model
    model.occ.synchronize()

    # if point is not None:
    #     gmsh.model.mesh.embed(0, [point_tag], 3, cyl)

    #     gmsh.model.mesh.field.add("Distance", 1)
    #     gmsh.model.mesh.field.setNumbers(1, "PointsList", [point_tag])
    #     gmsh.model.mesh.field.add("MathEval", 2)
    #     gmsh.model.mesh.field.setString(2, "F", "F1^1.2 + 0.001")
    #     gmsh.model.mesh.field.setAsBackgroundMesh(2)

    # model.mesh.setSize(model.getEntities(0), 20e-3)
    # Add physical marker for cells. It is important to call this function
    # after OpenCascade synchronization
    model.add_physical_group(dim=3, tags=[cyl])
    # Generate the mesh
    # gmsh.option.setNumber(
    #     "Mesh.CharacteristicLengthMin", min(HR_mesh_size, AR_mesh_size)
    # )
    # gmsh.option.setNumber(
    #     "Mesh.CharacteristicLengthMax", max(HR_mesh_size, AR_mesh_size)
    # )

    # Switch off these mesh sizings
    gmsh.option.setNumber("Mesh.MeshSizeExtendFromBoundary", 0)
    gmsh.option.setNumber("Mesh.MeshSizeFromPoints", 0)
    gmsh.option.setNumber("Mesh.MeshSizeFromCurvature", 0)

    gmsh.option.setNumber("Mesh.Algorithm", mesh_algorithm)

    # Use a callback to specify what the mesh size changes
    # base on distance from the HR surface
    def meshSizeCallback(dim, tag, x, y, z, lc):
        bulk = (
            HR_mesh_size
            + (AR_mesh_size - HR_mesh_size) * abs(z - thickness / 2) / thickness
        )
        if point is not None:
            d = cdist([[*point, thickness / 2]], [[x, y, z]]).T[0]
            pabs = d**1.2 + 0.0005
        else:
            pabs = lc
        return min(lc, min(bulk, pabs))

    gmsh.model.mesh.setSizeCallback(meshSizeCallback)

    if save_filename is not None:
        gmsh.write(save_filename)

    model.mesh.generate(dim=3)

    # Create a DOLFINx mesh (same mesh on each rank)
    msh, cell_markers, facet_markers = gmshio.model_to_mesh(model, MPI.COMM_SELF, 0)
    msh.name = name
    cell_markers.name = f"{msh.name}_cells"
    facet_markers.name = f"{msh.name}_facets"
    gmsh.finalize()

    return msh, cell_markers, facet_markers


def sweep_cylinder_3D(
    radius,
    thickness,
    num_elements,
    heights,
    add_flats=False,
    flat_hw=0.3265 / 2,
    HR_mesh_size=0.02,
    AR_mesh_size=0.02,
    *,
    mesh_algorithm=0,
    name="sweep_cyclinder",
    save_filename=None,
):
    """Creates a 3-dimensional sweep-cylinder mesh with a HR (z=+thickness) and AR
    (z=-thickness) surface.

    Parameters
    ----------
    radius, thickness : float
        Radius and thickness of cylinder in meters
    num_elements:vector of integers
        Number of elements on each layer in extrusionof mesh
        e.g. [(3, 5, 15 )] represents three layers, with
        3, 5, 15 elements each
    heights: vector of double
        Fractional heights of each layer
    add_flats: boolean
        Add flat faces to the sides of test mass
    flat_hw: float
        test mass halfwidth at center where the side flats are
        (only apply if add_flats is set to True)
        Default: 0.16325
    HR_mesh_size, AR_mesh_size : float
        Sets target mesh size at each surface in units of meters
    name : str, optional
        Name of cylinder in the geometry
    save_filename : str
        Name and path of file to save geometry too if needed, if None not saved.

    Returns
    -------
    model : gmsh.model()
    cell_markers : dolfinx.cpp.mesh.MeshTags_int32
    facet_markers : dolfinx.mesh.MeshTagsMetaClass
    """
    gmsh.initialize()
    # Choose if Gmsh output is verbose
    gmsh.option.setNumber("General.Terminal", 0)
    model = gmsh.model()

    model.add(name)
    model.setCurrent(name)

    disc = model.occ.addDisk(
        0, 0, -thickness / 2, radius, radius, tag=1, zAxis=[0, 0, 1]
    )
    # cyl = model.occ.addCylinder(0, 0, -thickness / 2, 0, 0, thickness, radius, tag=1)

    if add_flats is True:
        dBox = radius - flat_hw

        # Add boxes on the sides to subtract:
        model.occ.add_rectangle(
            -radius - dBox,
            -radius,
            -thickness / 2,
            dBox * 2,
            2 * radius,
            tag=2,
        )
        model.occ.add_rectangle(
            radius - dBox,
            -radius,
            -thickness / 2,
            dBox * 2,
            2 * radius,
            tag=3,
        )

        model.occ.cut([(2, 1)], [(2, 2), (2, 3)], 4)
        disc = 4

    # Extrude mesh
    ov = model.occ.extrude(
        [(2, disc)], 0, 0, thickness, numElements=num_elements, heights=heights
    )
    cyl = ov[1][1]
    # Synchronize OpenCascade representation with gmsh model
    model.occ.synchronize()
    # model.mesh.setSize(model.getEntities(0), 20e-3)
    # Add physical marker for cells. It is important to call this function
    # after OpenCascade synchronization
    model.add_physical_group(3, [cyl], 1)
    # Generate the mesh
    gmsh.option.setNumber(
        "Mesh.CharacteristicLengthMin", min(HR_mesh_size, AR_mesh_size)
    )
    gmsh.option.setNumber(
        "Mesh.CharacteristicLengthMax", max(HR_mesh_size, AR_mesh_size)
    )

    # Switch off these mesh sizings
    gmsh.option.setNumber("Mesh.MeshSizeExtendFromBoundary", 0)
    gmsh.option.setNumber("Mesh.MeshSizeFromPoints", 0)
    gmsh.option.setNumber("Mesh.MeshSizeFromCurvature", 0)

    gmsh.option.setNumber("Mesh.Algorithm", mesh_algorithm)  # Auto?

    # Use a callback to specify what the mesh size changes
    # base on distance from the HR surface
    def meshSizeCallback(dim, tag, x, y, z, lc):
        return min(
            lc,
            HR_mesh_size
            + (AR_mesh_size - HR_mesh_size) * abs(z - thickness / 2) / thickness,
        )

    gmsh.model.mesh.setSizeCallback(meshSizeCallback)

    if save_filename is not None:
        gmsh.write(save_filename)

    model.mesh.generate(dim=3)

    # Create a DOLFINx mesh (same mesh on each rank)
    msh, cell_markers, facet_markers = gmshio.model_to_mesh(model, MPI.COMM_SELF, 0)
    msh.name = name
    cell_markers.name = f"{msh.name}_cells"
    facet_markers.name = f"{msh.name}_facets"
    gmsh.finalize()

    return msh, cell_markers, facet_markers
