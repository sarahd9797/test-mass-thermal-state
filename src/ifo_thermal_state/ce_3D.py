from dolfinx.mesh import locate_entities, meshtags
from dolfinx.fem.petsc import (
    LinearProblem,
    assemble_matrix,
    create_vector,
    assemble_vector,
)
from dolfinx.fem import FunctionSpace, Function, Constant, form
from ifo_thermal_state.mesh import sweep_cylinder_3D
from .fea import BoundaryCondition, evaluate_solution, build_nullspace
from petsc4py import PETSc
import numpy as np
import ufl
from .math import composite_newton_cotes_weights
from types import SimpleNamespace
from tqdm import tqdm
import dolfinx
from slepc4py import SLEPc
import slepc4py
import sys

if dolfinx.__version__ < "0.7":
    from dolfinx.fem import VectorFunctionSpace

    def get_function_space(mesh, family, degree):
        return VectorFunctionSpace(mesh, (family, degree))

else:

    def get_function_space(mesh, family, degree):
        return FunctionSpace(mesh, (family, degree, (mesh.geometry.dim,)))


class CEBeamsplitter3D:
    """
    Attributes
    ----------


    """

    def __init__(self):
        # Fused silica material properties:
        self.rho = 2202  # density [kg/m^3]
        self.kappa = 1.38  # thermal conductivity [W/(m K)]
        self.C = 745  # specific heat [J/(kg K)]
        self.eps = 0.93  # emissivity of fused silica
        self.sigm = 5.670e-8  # Stefan-Boltzmann constnat [W/(m^2 K^4)]
        self.Text = 293.15  # External temperature [K]
        self.E = 72e9  # Young's modulus
        self.nu_ = 0.17  # Poisson ratio
        self.alpha = 0.55e-6  # Thermal expansion coefficient [1/K]
        self.n = 1.4496  # Refractive index at 1064 nm
        self.dndT = 8.6e-6  # refractive index change vs Temperature
        self.alpha_BS = 0.5e-6  # BS coating absorption coefficient
        self.alpha_AR = 1.5e-6  # AR coating absorption coefficient
        self.gamma = 20e-6  # Estimation of FS bul absorption (1/m)

    def epsilon(self, u):
        """Return symmetric strain tensor (assuming isotropic domain) from
        displacement."""
        return ufl.sym(
            ufl.grad(u)
        )  # Equivalent to 0.5*(ufl.nabla_grad(u) + ufl.nabla_grad(u).T)

    def sigma(self, u, dT):
        """Return stress from displacement and temperature change."""
        return (
            self.lmbda * ufl.tr(self.epsilon(u))
            - self.alpha * (3 * self.lmbda + 2 * self.mu_) * dT
        ) * ufl.Identity(3) + 2.0 * self.mu_ * self.epsilon(u)

    def sigma_noT(self, u):
        return self.lmbda * ufl.nabla_div(u) * ufl.Identity(
            len(u)
        ) + 2 * self.mu_ * self.epsilon(u)

    @property
    def eta(self):
        return -4 * self.eps * self.sigm * self.Text**3

    @property
    def mu_(self):
        """Lame coefficient."""
        return self.E / 2 / (1 + self.nu_)

    @property
    def lmbda(self):
        return self.E * self.nu_ / (1 + self.nu_) / (1 - 2 * self.nu_)


class CE3DBeamSplitterSteadyState:
    """
    Attributes
    ----------
    aoi :   float
            angle of incidence of the beamsplitter
    w0  :   float
            beam radius
    x0  :   float
            incidence position offset

    """

    def __init__(self, model, aoi, w0, CG=2):
        self.model = model
        # Namespace to hold all the temperature solving pieces
        self.temperature = SimpleNamespace()
        ds = self.temperature.ds = ufl.Measure(
            "ds", domain=self.model.msh, subdomain_data=self.model.facet_tag
        )
        #  angle of incidence
        self.aoi = Constant(model.msh, PETSc.ScalarType(aoi))
        # beamsizes
        self.w0 = Constant(model.msh, PETSc.ScalarType(w0))
        # position of incidence on BS side:
        self.x0 = Constant(model.msh, PETSc.ScalarType(0))
        # Estimate power incidet on BS:
        Parm = 1.5e6  # 1.5 MW
        arm_finesse = 450
        itm_T = 1.4e-2
        ARG = itm_T * arm_finesse**2 / (np.pi**2 * np.sqrt(1 - itm_T))
        self.P_bs = Constant(model.msh, PETSc.ScalarType(Parm / ARG * 2))

        V = self.temperature.V = FunctionSpace(self.model.msh, ("CG", CG))
        u = self.temperature.u = ufl.TrialFunction(V)
        v = self.temperature.v = ufl.TestFunction(V)
        self.temperature.uh = Function(V)
        I_BS = self.temperature.I_BS = Function(V)
        I_AR = self.temperature.I_AR = Function(V)
        Q_SUB = self.temperature.Q_SUB = Function(V)

        self.temperature.boundary_conditions = [
            BoundaryCondition(
                "Robin",
                1,
                (self.model.eta / self.model.kappa, -I_BS / self.model.eta),
                u,
                v,
                ds,
            ),  # BS
            BoundaryCondition(
                "Robin",
                2,
                (self.model.eta / self.model.kappa, -I_AR / self.model.eta),
                u,
                v,
                ds,
            ),  # AR
            BoundaryCondition(
                "Robin",
                3,
                (self.model.eta / self.model.kappa, 0),
                u,
                v,
                ds,
            ),  # Barrel
        ]
        self.temperature.F = (
            -ufl.inner(ufl.nabla_grad(u), ufl.nabla_grad(v))
        ) * ufl.dx + (1 / self.model.kappa) * ufl.inner(Q_SUB, v) * ufl.dx

        for condition in self.temperature.boundary_conditions:
            self.temperature.F += condition.bc

        self.temperature.a = form(ufl.lhs(self.temperature.F))
        self.temperature.L = form(ufl.rhs(self.temperature.F))
        self.temperature.A = assemble_matrix(self.temperature.a)
        self.temperature.A.assemble()
        self.temperature.b = create_vector(self.temperature.L)

        self.temperature.solver = PETSc.KSP().create(model.msh.comm)
        self.temperature.solver.setOperators(self.temperature.A)
        self.temperature.solver.setType(PETSc.KSP.Type.CG)
        self.temperature.solver.getPC().setType(PETSc.PC.Type.GAMG)

        # Namespace to hold all the temperature solving pieces
        self.deformation = SimpleNamespace()
        self.deformation.V = get_function_space(self.model.msh, "CG", CG)
        self.deformation.u = ufl.TrialFunction(self.deformation.V)
        self.deformation.v = ufl.TestFunction(self.deformation.V)
        self.deformation.uh = Function(self.deformation.V)
        self.deformation.T = self.temperature.uh
        # Create the near-nullspace and attach it to the PETSc matrix:
        self.deformation.ns = build_nullspace(self.deformation.V)
        self.deformation.F = (
            ufl.inner(
                self.model.sigma(self.deformation.u, self.deformation.T),
                self.model.epsilon(self.deformation.v),
            )
            * ufl.dx
        )
        self.deformation.a = form(ufl.lhs(self.deformation.F))
        self.deformation.L = form(ufl.rhs(self.deformation.F))
        self.deformation.A = assemble_matrix(self.deformation.a)
        self.deformation.A.assemble()
        self.deformation.A.setNullSpace(self.deformation.ns)
        self.deformation.A.setNearNullSpace(self.deformation.ns)
        self.deformation.b = create_vector(self.deformation.L)

        self.deformation.solver = PETSc.KSP().create(model.msh.comm)
        self.deformation.solver.setOperators(self.deformation.A)
        self.deformation.solver.setType(PETSc.KSP.Type.CG)
        self.deformation.solver.getPC().setType(PETSc.PC.Type.GAMG)

    def I_BS(self, x):
        """Intensity on BS side."""
        P_BS_SURF = self.model.alpha_BS * self.P_bs.value
        I0 = 2 * P_BS_SURF * np.cos(self.aoi.value) / (np.pi * self.w0.value**2)
        x_BS = (x[0] - self.x0.value) * np.cos(self.aoi.value)
        return I0 * np.exp(-2 * (x_BS**2 + x[1] ** 2) / self.w0.value**2)

    def I_AR(self, x):
        """Intensity on AR side."""
        P_AR_SURF = self.model.alpha_AR * self.P_bs.value / 2
        theta_t = np.arcsin(1 * np.sin(self.aoi.value) / self.model.n)
        x0_AR = np.tan(theta_t) * self.model.thickness - self.x0.value
        I0 = 2 * P_AR_SURF * np.cos(self.aoi.value) / (np.pi * self.w0.value**2)
        x_AR = (x[0] - x0_AR) * np.cos(self.aoi.value)
        return I0 * np.exp(-2 * (x_AR**2 + x[1] ** 2) / self.w0.value**2)

    def Q_SUB(self, x):
        """Volumetric heat source."""
        P_BS_SUB = self.model.gamma * self.model.thickness * self.P_bs.value / 2
        theta_t = np.arcsin(1 * np.sin(self.aoi.value) / self.model.n)
        x0_AR = np.tan(theta_t) * self.model.thickness - self.x0.value
        x0_midBS = x0_AR / 2
        x_beam = (x[0] - x0_midBS) * np.cos(theta_t) + x[2] * np.sin(theta_t)
        I0 = 2 * P_BS_SUB * np.cos(theta_t) / (np.pi * self.w0.value**2)
        return (
            I0
            * np.exp(-2 * (x_beam**2 + x[1] ** 2) / self.w0.value**2)
            / self.model.thickness
        )

    def solve_parametric(self, parameter, parameter_values):
        """Run parametric study of some parameter with a set of parameter values."""

        def solve(state, uh):
            with state.b.localForm() as loc_b:
                loc_b.set(0)
            assemble_vector(state.b, state.L)
            state.solver.solve(state.b, state.uh.vector)
            uh.x.scatter_forward()

        if not hasattr(self, parameter):
            raise ValueError(f"Parameter {parameter} does not exist")
        else:
            sols_temperature = []
            sols_deformation = []
            for ival in tqdm(parameter_values):
                exec(f"self.{parameter}.value = ival")

                # Solving
                self.temperature.I_BS.interpolate(self.I_BS)
                self.temperature.I_AR.interpolate(self.I_AR)
                self.temperature.Q_SUB.interpolate(self.Q_SUB)

                solve(self.temperature, self.temperature.uh)
                solve(self.deformation, self.deformation.uh)
                sols_temperature.append(self.temperature.uh.copy())
                sols_deformation.append(self.deformation.uh.copy())

            self.temperature.solution = sols_temperature
            self.deformation.solution = sols_deformation
            return self.temperature.solution, self.deformation.solution

    def solve_temperature(self, options={}):
        self.temperature.I_BS.interpolate(self.I_BS)
        self.temperature.I_AR.interpolate(self.I_AR)
        self.temperature.Q_SUB.interpolate(self.Q_SUB)
        with self.temperature.b.localForm() as loc_b:
            loc_b.set(0)
        assemble_vector(self.temperature.b, self.temperature.L)
        self.temperature.solver.solve(self.temperature.b, self.temperature.uh.vector)
        self.temperature.uh.x.scatter_forward()
        self.temperature.solution = [self.temperature.uh.copy()]
        return self.temperature.solution

    def solve_deformation(self, options={}):
        with self.deformation.b.localForm() as loc_b:
            loc_b.set(0)
        assemble_vector(self.deformation.b, self.deformation.L)
        self.deformation.solver.solve(self.deformation.b, self.deformation.uh.vector)
        self.deformation.uh.x.scatter_forward()
        self.deformation.solution = [self.deformation.uh.copy()]
        return self.deformation.solution

    def get_opd_coordinates(self, aoi, x0=0, path="both", Nx=201, Ny=201, Nz=10):
        """Get coordinates to evaluate opd through the beam splitter:

        Parameters:
        ------------------------
        aoi : float
            angle of incidence
        x0 : float
            position offset on BS surface, default: 0
        path : str
            "michx" or "sec" or "both", default: "both""
        aoi_unit: str
            unit of angle of incidence, default: radians

        Returns:
        -------------------------
        x3, y3, z3 :  3D grid of x, y, z coordinate
        dz
        """
        _y1 = np.linspace(-self.model.radius, self.model.radius, Ny)
        _y3 = np.tile(_y1[:, np.newaxis, np.newaxis], (1, Nx, Nz))
        _z1 = np.linspace(-self.model.thickness / 2, self.model.thickness / 2, Nz)
        _z3 = np.tile(_z1[np.newaxis, np.newaxis, :], (Ny, Nx, 1))

        if path == "michx":
            theta_in = np.array([aoi])
        elif path == "sec":
            theta_in = np.array([-aoi])
        elif path == "both":
            theta_in = np.array([aoi, -aoi])

        xs = []
        dzs = []
        for iT in theta_in:
            theta_t = np.arcsin(1 * np.sin(iT) / self.model.n)
            xAR = np.tan(theta_t) * self.model.thickness + x0
            _x1 = np.linspace(-self.model.radius, self.model.radius, Nx)
            _x3_0 = np.tile(_x1[np.newaxis, :, np.newaxis], (Ny, 1, Nz))
            dx1_tilt = self.model.thickness * np.tan(-theta_t) * np.linspace(0, 1, Nz)
            _x3 = (
                _x3_0 + xAR + np.tile(dx1_tilt[np.newaxis, np.newaxis, :], (Ny, Nx, 1))
            )
            dz = (_z1[1] - _z1[0]) / np.cos(theta_t)
            xs.append(_x3)
            dzs.append(dz)

        if path == "michx" or path == "sec":
            return _x3, _y3, _z3, dz
        elif path == "both":
            michx = {}
            sec = {}
            michx["x"] = xs[0]
            michx["y"] = _y3
            michx["z"] = _z3
            michx["dz"] = dzs[0]

            sec["x"] = xs[1]
            sec["y"] = _y3
            sec["z"] = _z3
            sec["dz"] = dzs[1]
            return michx, sec

    def evaluate_temperature(self, x, y, z, *, meshgrid=False):
        """Evaluates the temperature solution at various coordinates throughout the
        mesh.

        Parameters
        ----------
        x, y, z : arraylike
            If meshgrid is True, these should be 1D vectors describing the
            volume of points to evaluate the solution over. These inputs
            will be inputted to meshgrid to determine what points to evaluate.
            If meshgrid is False then x,y,z describe the array of points to
            evaluate at. If False x,y,z must be the same length or singular
            values.
        meshgrid : bool, optional
            Whether to use meshgrid to project the provided x,y,z values
            to a grid on which to evaluate

        Returns
        -------
            points : arraylike
                The x, y, z positions of evaluated points. If meshgrid is
                True the shape is (x.size, y.size, z.size, 3), otherwise
                it has a shape of (max(x.size, y.size, z.size), 1).
            result : arraylike
                The evaluated points [K]. If meshgrid is True the shape is
                (x.size, y.size, z.size, 3), otherwise it has a shape of
                (max(x.size, y.size, z.size), 1).
            mask : arraylike[bool]
                A boolean mask, False values are points which could not be
                evaluated, typically those outside of the mesh
        """
        outs = []
        for i in range(len(self.temperature.solution)):
            outs.append(
                evaluate_solution(
                    self.model.msh,
                    x,
                    y,
                    z,
                    self.temperature.solution[i],
                    meshgrid=meshgrid,
                )
            )

        return outs

    def evaluate_deformation(self, x, y, z, *, meshgrid=False):
        """Evaluates the thermo-elastic deformation solution at various coordinates
        throughout the mesh.

        Parameters
        ----------
        x, y, z : arraylike
            If meshgrid is True, these should be 1D vectors describing the
            volume of points to evaluate the solution over. These inputs
            will be inputted to meshgrid to determine what points to evaluate.
            If meshgrid is False then x,y,z describe the array of points to
            evaluate at. If False x,y,z must be the same length or singular
            values.
        meshgrid : bool, optional
            Whether to use meshgrid to project the provided x,y,z values
            to a grid on which to evaluate

        Returns
        -------
            points : arraylike
                The x, y, z positions of evaluated points. If meshgrid is
                True the shape is (x.size, y.size, z.size, 3), otherwise
                it has a shape of (max(x.size, y.size, z.size), 3).
            result : arraylike
                The evaluated points [m]. If meshgrid is True the shape is
                (x.size, y.size, z.size, 3), otherwise it has a shape of
                (max(x.size, y.size, z.size), 3).
            mask : arraylike[bool]
                A boolean mask, False values are points which could not be
                evaluated, typically those outside of the mesh
        """
        outs = []
        for i in range(len(self.temperature.solution)):
            outs.append(
                evaluate_solution(
                    self.model.msh,
                    x,
                    y,
                    z,
                    self.deformation.solution[i],
                    meshgrid=meshgrid,
                )
            )
        return outs

    def evaluate_surface_deformation(self, Nx=201, Ny=201):
        """Evaluate the surface deformation.

        Parameters:
        -----------------------------
        Nx, Ny: Number of sample points in x and y direction

        Returns:
        ----------------------------
        bs, ar : dictionary, cosits of
                x, y:  1d x,y coordinate
                deform: deformation map of bs, ar surface
        """
        bs = {}
        ar = {}
        x, y, z = (
            np.linspace(-self.model.radius, self.model.radius, Nx),
            np.linspace(-self.model.radius, self.model.radius, Ny),
            [-self.model.thickness / 2, self.model.thickness / 2],
        )
        bs["x"] = x
        ar["x"] = x
        bs["y"] = y
        ar["y"] = y
        ar["deform"] = []
        bs["deform"] = []
        data = self.evaluate_deformation(x, y, z, meshgrid=True)
        for iD in data:
            _wAR = iD[1][:, :, 0, 2]
            _wBS = iD[1][:, :, 1, 2]
            ar["deform"].append(-(_wAR - np.nanmin(_wAR)))
            bs["deform"].append(_wBS - np.nanmax(_wBS))
        return bs, ar

    def evaluate_thermal_lens(self, aoi, Nx=201, Ny=201, Nz=10):
        """Evaluate  thermal lens opd through the beamsplitter at some angle of icidenc
        aoi.

        Parameters:
        -----------------------------------------
        aoi :   scalar or arraylike
                angle of incidence. If aoi is arraylike, it's assumed to match
                the number of values in parameter sweep solution
        Nx, Ny, Nz: int
                Number of points evaluated at along each dimension

        Returns:
        -----------------------------------
        michx, sec: dictionary
                Each direction contains:
                x, y: 1D corrdinate
                opd: 2D thermal lens map
        """
        weights = composite_newton_cotes_weights(Nz, 5)

        def evaluate(x, y, z, dz, solution, meshgrid=False):
            _, dT, _ = evaluate_solution(
                self.model.msh, x, y, z, solution, meshgrid=meshgrid
            )
            dT = dT[:, 0].reshape(x.shape)
            opd = self.model.dndT * dz * np.sum(dT * weights[None, None, :], axis=2)
            x2 = x[:, :, -1]
            y2 = y[:, :, -1]
            x1 = x2[0]
            y1 = y2[:, 0]
            return x1, y1, opd

        michx = {}
        sec = {}
        michx["x"] = []
        michx["y"] = []
        michx["opd"] = []
        sec["x"] = []
        sec["y"] = []
        sec["opd"] = []
        # Check that the given aoi is an array or a scalar
        if hasattr(aoi, "__len__"):
            if len(aoi) == len(self.temperature.solution):
                for iA, iT in tqdm(zip(aoi, self.temperature.solution)):
                    michx_coord, sec_coord = self.get_opd_coordinates(
                        iA, path="both", Nx=Nx, Ny=Ny, Nz=Nz
                    )
                    _x_michx, _y_michx, _opd_michx = evaluate(
                        michx_coord["x"],
                        michx_coord["y"],
                        michx_coord["z"],
                        michx_coord["dz"],
                        iT,
                    )
                    michx["opd"].append(_opd_michx - np.nanmax(_opd_michx))
                    _x_sec, _y_sec, _opd_sec = evaluate(
                        sec_coord["x"],
                        sec_coord["y"],
                        sec_coord["z"],
                        sec_coord["dz"],
                        iT,
                    )
                    sec["opd"].append(_opd_sec - np.nanmax(_opd_sec))
            else:
                raise ValueError(
                    "length of input aoi does not match length of solutions"
                )
        else:
            for iT in tqdm(self.temperature.solution):
                michx_coord, sec_coord = self.get_opd_coordinates(
                    aoi, path="both", Nx=Nx, Ny=Ny, Nz=Nz
                )
                _x_michx, _y_michx, _opd_michx = evaluate(
                    michx_coord["x"],
                    michx_coord["y"],
                    michx_coord["z"],
                    michx_coord["dz"],
                    iT,
                )

                michx["opd"].append(_opd_michx - np.nanmax(_opd_michx))
                _x_sec, _y_sec, _opd_sec = evaluate(
                    sec_coord["x"], sec_coord["y"], sec_coord["z"], sec_coord["dz"], iT
                )
                sec["x"].append(_x_sec)
                sec["y"].append(_y_sec)
                sec["opd"].append(_opd_sec - np.nanmax(_opd_sec))
        michx["x"] = _x_michx
        michx["y"] = _y_michx
        sec["x"] = _x_sec
        sec["y"] = _y_sec
        return michx, sec


class CE3DBeamSplitterEigenfrequency:
    def __init__(self, model, CG=2):
        slepc4py.init(sys.argv)
        self.model = model

        # Namespace to hold all the eigenfrequency study solving pieces:
        self.eigen = SimpleNamespace()

        V = self.eigen.V = get_function_space(self.model.msh, "CG", CG)
        self.eigen.u = ufl.TrialFunction(V)
        self.eigen.v = ufl.TestFunction(V)

    def solve_eigenfrequencies(self, N_modes=10, solver_kwargs={}):
        "Solve eigen frequencies problem"
        self.eigen.N_modes = N_modes
        a = form(
            ufl.inner(
                self.model.sigma_noT(self.eigen.u),
                self.model.epsilon(self.eigen.v),
            )
            * ufl.dx
        )
        m = form(self.model.rho * ufl.inner(self.eigen.u, self.eigen.v) * ufl.dx)

        A = self.eigen.A = assemble_matrix(a, [])
        A.assemble()

        M = self.eigen.M = assemble_matrix(m, [])
        M.assemble()

        eigensolver = self.get_eigensolver(**solver_kwargs)
        eigensolver.solve()
        evs = eigensolver.getConverged()
        vr, vi = A.getVecs()
        u_output = Function(self.eigen.V)

        solution = self.eigen.solution = {}
        eigenfreq = solution["eigenfrequencies"] = []
        eigenvecs = solution["eigenvectors"] = []

        if evs > 0:
            for i in range(min(self.eigen.N_modes, evs)):
                l = eigensolver.getEigenpair(i, vr, vi)
                eigenfreq.append(np.sqrt(l.real) / 2 / np.pi)
                u_output.x.array[:] = vr
                eigenvecs.append(u_output.x.array[:].copy())

    def get_eigensolver(
        self,
        problem_type=SLEPc.EPS.ProblemType.GHEP,
        st_type=SLEPc.ST.Type.SINVERT,
        st_shift=0,
        whicheigenpairs=SLEPc.EPS.Which.TARGET_REAL,
        tol=1e-9,
    ):
        eigensolver = SLEPc.EPS().create(self.model.msh.comm)
        eigensolver.setDimensions(self.eigen.N_modes)
        eigensolver.setProblemType(problem_type)
        eigensolver.setTolerances(tol=tol)

        st = SLEPc.ST().create(self.model.msh.comm)
        st.setType(st_type)
        st.setShift(st_shift)
        st.setFromOptions()

        eigensolver.setST(st)
        eigensolver.setWhichEigenpairs(whicheigenpairs)
        eigensolver.setOperators(self.eigen.A, self.eigen.M)
        eigensolver.setFromOptions()

        return eigensolver


def make_beamsplitter_model(
    radius, thickness, mesh_function=sweep_cylinder_3D, mesh_function_kwargs={}
):
    model = CEBeamsplitter3D()
    model.radius = radius
    model.thickness = thickness

    add_flats = False

    mesh_function_kwargs["HR_mesh_size"] = mesh_function_kwargs["BS_mesh_size"]
    mesh_function_kwargs.pop("BS_mesh_size")

    if "add_flats" in mesh_function_kwargs:
        add_flats = mesh_function_kwargs["add_flats"]
        if "flat_hw" in mesh_function_kwargs:
            flat_hw = mesh_function_kwargs["flat_hw"]
        else:
            raise ValueError(
                "'flat_hw' need to be provided if 'add_flats' is set to True"
            )

    if "num_elements" not in mesh_function_kwargs:
        mthickness_surface = 0.002
        mthickness_mid = 0.005
        thickness_surf = 0.01
        num_element_surf = int(thickness_surf / mthickness_surface)
        num_element_mid = int((model.thickness - 2 * thickness_surf) / mthickness_mid)
        mesh_function_kwargs["num_elements"] = [
            num_element_surf,
            num_element_mid,
            num_element_surf,
        ]
        mesh_function_kwargs["heights"] = [
            thickness_surf / model.thickness,
            (model.thickness - thickness_surf) / model.thickness,
            1,
        ]

    model.msh, model.cell_markers, model.facet_markers = mesh_function(
        model.radius, model.thickness, **mesh_function_kwargs
    )

    # Marking boundaries
    def boundary_BS(x):
        """Return geometry entities close to HR surface."""
        return np.isclose(x[2], model.thickness / 2)

    def boundary_AR(x):
        """Return geometry entities close to AR surface."""
        return np.isclose(x[2], -model.thickness / 2)

    def boundary_barrel(x):
        """Return geometry entities close to barrel surface."""
        if add_flats is False:
            bb = np.isclose(x[0] ** 2 + x[1] ** 2, model.radius**2)
        else:
            bb = np.bitwise_or(
                np.isclose(x[0] ** 2 + x[1] ** 2, model.radius**2),
                np.isclose(abs(x[0]), flat_hw),
            )
        return bb

    boundaries = [
        (1, boundary_BS),
        (2, boundary_AR),
        (3, boundary_barrel),
    ]
    # We now loop through all the boundary conditions and create MeshTags
    # identifying the facets for each boundary condition.
    model.facet_indices, model.facet_markers = [], []
    fdim = model.msh.topology.dim - 1  # Facet dimension (2)

    for marker, locator in boundaries:
        facets = locate_entities(model.msh, fdim, locator)
        # Find facets
        model.facet_indices.append(facets)
        # Label facet markers
        model.facet_markers.append(np.full_like(facets, marker))

    model.facet_indices = np.hstack(model.facet_indices).astype(np.int32)
    model.facet_markers = np.hstack(model.facet_markers).astype(np.int32)
    model.sorted_facets = np.argsort(model.facet_indices)

    model.facet_tag = meshtags(
        model.msh,
        fdim,
        model.facet_indices[model.sorted_facets],
        model.facet_markers[model.sorted_facets],
    )

    model.msh.topology.create_connectivity(
        model.msh.topology.dim - 1, model.msh.topology.dim
    )
    return model


# Test mass class
class CETestMass3D:
    """
    Attributes
    ----------


    """

    def __init__(self):
        # Fused silica material properties:
        self.rho = 2202  # density [kg/m^3]
        self.kappa = 1.38  # thermal conductivity [W/(m K)]
        self.C = 745  # specific heat [J/(kg K)]
        self.eps = 0.93  # emissivity of fused silica
        self.sigm = 5.670e-8  # Stefan-Boltzmann constnat [W/(m^2 K^4)]
        self.Text = 293.15  # External temperature [K]
        self.E = 72e9  # Young's modulus
        self.nu_ = 0.17  # Poisson ratio
        self.alpha = 0.55e-6  # Thermal expansion coefficient [1/K]
        self.n = 1.4496  # Refractive index at 1064 nm
        self.dndT = 8.6e-6  # refractive index change vs Temperature
        self.alpha_BS = 0.5e-6  # BS coating absorption coefficient
        self.alpha_AR = 0.7e-6  # AR coating absorption coefficient
        self.gamma = 20e-6  # Estimation of FS bul absorption (1/m)

    def epsilon(self, u):
        """Return symmetric strain tensor (assuming isotropic domain) from
        displacement."""
        return ufl.sym(
            ufl.grad(u)
        )  # Equivalent to 0.5*(ufl.nabla_grad(u) + ufl.nabla_grad(u).T)

    def sigma(self, u, dT):
        """Return stress from displacement and temperature change."""
        return (
            self.lmbda * ufl.tr(self.epsilon(u))
            - self.alpha * (3 * self.lmbda + 2 * self.mu_) * dT
        ) * ufl.Identity(3) + 2.0 * self.mu_ * self.epsilon(u)

    def sigma_noT(self, u):
        return self.lmbda * ufl.nabla_div(u) * ufl.Identity(
            len(u)
        ) + 2 * self.mu_ * self.epsilon(u)

    @property
    def eta(self):
        return -4 * self.eps * self.sigm * self.Text**3

    @property
    def mu_(self):
        """Lame coefficient."""
        return self.E / 2 / (1 + self.nu_)

    @property
    def lmbda(self):
        return self.E * self.nu_ / (1 + self.nu_) / (1 - 2 * self.nu_)


class CETestMass3DSteadyState:
    def __init__(self, model, CG=2):
        self.model = model
        # Namespace to hold all the temperature solving pieces
        self.temperature = SimpleNamespace()
        ds = self.temperature.ds = ufl.Measure(
            "ds", domain=self.model.msh, subdomain_data=self.model.facet_tag
        )

        V = self.temperature.V = FunctionSpace(self.model.msh, ("CG", CG))
        u = self.temperature.u = ufl.TrialFunction(V)
        v = self.temperature.v = ufl.TestFunction(V)
        I_HR = self.temperature.I_HR = Function(V)
        I_BR = self.temperature.I_BR = Function(V)
        I_TM_AR = self.temperature.I_TM_AR = Function(V)
        Q_SUB = self.temperature.Q_SUB = Function(V)

        self.temperature.boundary_conditions = [
            BoundaryCondition(
                "Robin",
                1,
                (self.model.eta / self.model.kappa, -I_HR / self.model.eta),
                u,
                v,
                ds,
            ),  # HR
            BoundaryCondition(
                "Robin",
                2,
                (self.model.eta / self.model.kappa, -I_TM_AR / self.model.eta),
                u,
                v,
                ds,
            ),  # AR
            BoundaryCondition(
                "Robin",
                3,
                (self.model.eta / self.model.kappa, -I_BR / self.model.eta),
                u,
                v,
                ds,
            ),  # Barrel
        ]

        self.temperature.F = (
            -self.model.kappa * ufl.inner(ufl.nabla_grad(u), ufl.nabla_grad(v))
        ) * ufl.dx + ufl.inner(Q_SUB, v) * ufl.dx

        for condition in self.temperature.boundary_conditions:
            self.temperature.F += condition.bc

        self.temperature.a = ufl.lhs(self.temperature.F)
        self.temperature.L = ufl.rhs(self.temperature.F)

        # Namespace to hold all the temperature solving pieces
        self.deformation = SimpleNamespace()
        self.deformation.V = get_function_space(self.model.msh, "CG", CG)
        self.deformation.u = ufl.TrialFunction(self.deformation.V)
        self.deformation.v = ufl.TestFunction(self.deformation.V)

        # Create the near-nullspace and attach it to the PETSc matrix:
        self.deformation.ns = build_nullspace(self.deformation.V)

    def solve_temperature(self, options={}):
        """Solve linear variational problem."""
        options = {"pc_type": "gamg", **options}  # overwrite if provided by users

        problem = LinearProblem(
            self.temperature.a, self.temperature.L, petsc_options=options
        )
        self.temperature.solution = problem.solve()
        return self.temperature.solution

    def solve_deformation(self, options={}):
        options = {
            "ksp_type": "cgs",
            "ksp_rtol": 1.0e-10,
            "pc_type": "gamg",
            "mg_levels_ksp_type": "chebyshev",
            "mg_levels_pc_type": "jacobi",
            "mg_levels_esteig_ksp_type": "cgs",
            "mg_levels_ksp_chebyshev_esteig_steps": 40,
            "mg_coarse_pc_type": "jacobi",  # jacobi coarse solver for singular problem
            **options,  # overwrites any defaults
        }

        self.deformation.F = (
            ufl.inner(
                self.model.sigma(self.deformation.u, self.temperature.solution),
                self.model.epsilon(self.deformation.v),
            )
            * ufl.dx
        )
        self.deformation.a = ufl.lhs(self.deformation.F)
        self.deformation.L = ufl.rhs(self.deformation.F)

        problem = LinearProblem(
            self.deformation.a, self.deformation.L, bcs=[], petsc_options=options
        )
        problem.A.setNullSpace(self.deformation.ns)
        problem.A.setNearNullSpace(self.deformation.ns)

        self.deformation.solution = problem.solve()
        return self.deformation.solution


def make_test_mass_model(mesh_function=sweep_cylinder_3D, mesh_function_kwargs={}):
    model = CETestMass3D()
    model.radius = 0.3
    model.thickness = 0.5
    flat_hw = 0.5865 / 2

    if "add_flats" in mesh_function_kwargs:
        add_flats = mesh_function_kwargs["add_flats"]
        mesh_function_kwargs["flat_hw"] = flat_hw
    else:
        mesh_function_kwargs["add_flats"] = add_flats
        mesh_function_kwargs["flat_hw"] = flat_hw

    surf_thickness_ratio = 0.2
    thickness_surf = surf_thickness_ratio * model.thickness
    num_element_surf = 4
    num_element_mid = 8
    mesh_function_kwargs["num_elements"] = [
        num_element_surf,
        num_element_mid,
        num_element_surf,
    ]
    mesh_function_kwargs["heights"] = [
        thickness_surf / model.thickness,
        (model.thickness - thickness_surf) / model.thickness,
        1,
    ]

    model.msh, model.cell_markers, model.facet_markers = mesh_function(
        model.radius, model.thickness, **mesh_function_kwargs
    )

    # Marking boundaries
    def boundary_HR(x):
        """Return geometry entities close to HR surface."""
        return np.isclose(x[2], model.thickness / 2)

    def boundary_AR(x):
        """Return geometry entities close to AR surface."""
        return np.isclose(x[2], -model.thickness / 2)

    def boundary_barrel(x):
        """Return geometry entities close to barrel surface."""
        if add_flats is False:
            bb = np.isclose(x[0] ** 2 + x[1] ** 2, model.radius**2)
        else:
            bb = np.bitwise_or(
                np.isclose(x[0] ** 2 + x[1] ** 2, model.radius**2),
                np.isclose(abs(x[0]), flat_hw),
            )
        return bb

    boundaries = [
        (1, boundary_HR),
        (2, boundary_AR),
        (3, boundary_barrel),
    ]
    # We now loop through all the boundary conditions and create MeshTags
    # identifying the facets for each boundary condition.
    model.facet_indices, model.facet_markers = [], []
    fdim = model.msh.topology.dim - 1  # Facet dimension (2)

    for marker, locator in boundaries:
        facets = locate_entities(model.msh, fdim, locator)
        # Find facets
        model.facet_indices.append(facets)
        # Label facet markers
        model.facet_markers.append(np.full_like(facets, marker))

    model.facet_indices = np.hstack(model.facet_indices).astype(np.int32)
    model.facet_markers = np.hstack(model.facet_markers).astype(np.int32)
    model.sorted_facets = np.argsort(model.facet_indices)

    model.facet_tag = meshtags(
        model.msh,
        fdim,
        model.facet_indices[model.sorted_facets],
        model.facet_markers[model.sorted_facets],
    )

    model.msh.topology.create_connectivity(
        model.msh.topology.dim - 1, model.msh.topology.dim
    )
    return model
