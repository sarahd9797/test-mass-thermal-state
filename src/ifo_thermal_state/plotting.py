import pyvista
import dolfinx
import matplotlib.pyplot as plt

if dolfinx.__version__ < "0.7":
    from dolfinx.plot import create_vtk_mesh

    def get_vtk_mesh(input):
        if type(input) == dolfinx.mesh.Mesh:
            return create_vtk_mesh(input, input.topology.dim)
        elif type(input) == dolfinx.fem.function.FunctionSpaceBase:
            return create_vtk_mesh(input)

else:
    from dolfinx.plot import vtk_mesh

    def get_vtk_mesh(input):
        if type(input) == dolfinx.mesh.Mesh:
            return vtk_mesh(input, input.topology.dim)
        if type(input) == dolfinx.fem.function.FunctionSpaceBase:
            return vtk_mesh(input)


def plot_mesh(msh, cell_markers):
    plotter = pyvista.Plotter(notebook=True)
    grid = pyvista.UnstructuredGrid(*get_vtk_mesh(msh))
    num_local_cells = msh.topology.index_map(msh.topology.dim).size_local
    grid.cell_data["Marker"] = cell_markers.values[
        cell_markers.indices < num_local_cells
    ]
    grid.set_active_scalars("Marker")
    plotter.add_mesh(grid, show_edges=True)
    plotter.view_xy()
    plotter.show(jupyter_backend="trame")


def plot_deformation(Vu, u_sol, factor=1e6, warp_by="vector"):
    # Create plotter and pyvista grid
    p = pyvista.Plotter(notebook=True)
    topology, cell_types, geometry = get_vtk_mesh(Vu)
    grid = pyvista.UnstructuredGrid(topology, cell_types, geometry)

    if warp_by == "vector":
        # Attach vector values to grid and warp grid by vector
        grid["u"] = u_sol.x.array.reshape((geometry.shape[0], 3))
        p.add_mesh(grid, style="wireframe", color="k")
        warped = grid.warp_by_vector("u", factor=factor)
    elif warp_by == "scalar":
        grid["u"] = u_sol.x.array.reshape((geometry.shape[0], 3))[:, :, 2]
        p.add_mesh(grid, style="wireframe", color="k")
        warped = grid.warp_by_scalar("u", factor=factor)

    p.add_mesh(warped, show_edges=True)
    p.show_axes()
    p.show(jupyter_backend="trame")


def plot_temperature(V, u_sol, mesh_edge=True, clip=False, cip_normal="y"):
    # Create plotter and pyvista grid
    cm = plt.cm.get_cmap("plasma", 50)
    p = pyvista.Plotter(notebook=True)
    topology, cell_types, geometry = get_vtk_mesh(V)
    grid = pyvista.UnstructuredGrid(topology, cell_types, geometry)

    # Attach vector values to grid and warp grid by vector
    grid.point_data["u"] = u_sol.x.array.real
    if clip is True:
        grid = grid.clip(normal="y")
    grid.set_active_scalars("u")
    p.add_mesh(grid, show_edges=mesh_edge, cmap=cm)
    p.view_xy()
    p.show_axes()
    p.show(jupyter_backend="trame")
