from dolfinx.mesh import locate_entities, meshtags
from dolfinx.fem.petsc import (
    LinearProblem,
    assemble_matrix,
    assemble_vector,
    create_vector,
    create_matrix,
)

from dolfinx.fem import FunctionSpace, Function, Constant, form

from petsc4py import PETSc

from ifo_thermal_state.mesh import cylinder_3D
from .fea import BoundaryCondition, evaluate_solution, build_nullspace

import numpy as np
import ufl

from types import SimpleNamespace

import dolfinx

if dolfinx.__version__ < "0.7":
    from dolfinx.fem import VectorFunctionSpace

    def get_function_space(mesh, family, degree):
        return VectorFunctionSpace(mesh, (family, degree))

else:

    def get_function_space(mesh, family, degree):
        return FunctionSpace(mesh, (family, degree, (mesh.geometry.dim,)))


class AdvancedLIGOTestMass3D:
    """
    Attributes
    ----------


    """

    def __init__(self):
        # Fused silica material properties:
        self.rho = 2202  # density [kg/m^3]
        self.kappa = 1.38  # thermal conductivity [W/(m K)]
        self.C = 745  # specific heat [J/(kg K)]
        self.eps = 0.93  # emissivity of fused silica
        self.sigm = 5.670e-8  # Stefan-Boltzmann constnat [W/(m^2 K^4)]
        self.Text = 293.15  # External temperature [K]
        self.P_coat = 1  # Power absorbed by coating [W]
        self.E = 72e9  # Young's modulus
        self.nu_ = 0.17  # Poisson ratio
        self.alpha = 0.55e-6  # Thermal expansion coefficient [1/K]
        self.dndT = 8.6e-6  # refractive index change vs Temperature

    def epsilon(self, u):
        """Return symmetric strain tensor (assuming isotropic domain) from
        displacement."""
        return ufl.sym(
            ufl.grad(u)
        )  # Equivalent to 0.5*(ufl.nabla_grad(u) + ufl.nabla_grad(u).T)

    def sigma(self, u, dT):
        """Return stress from displacement and temperature change."""
        return (
            self.lmbda * ufl.tr(self.epsilon(u))
            - self.alpha * (3 * self.lmbda + 2 * self.mu_) * dT
        ) * ufl.Identity(3) + 2.0 * self.mu_ * self.epsilon(u)

    def sigma_noT(self, u):
        return self.lmbda * ufl.nabla_div(u) * ufl.Identity(
            len(u)
        ) + 2 * self.mu_ * self.epsilon(u)

    @property
    def eta(self):
        return -4 * self.eps * self.sigm * self.Text**3

    @property
    def mu_(self):
        """Lame coefficient."""
        return self.E / 2 / (1 + self.nu_)

    @property
    def lmbda(self):
        return self.E * self.nu_ / (1 + self.nu_) / (1 - 2 * self.nu_)


class AdvancedLIGOTestMass3DSteadyState:
    def __init__(self, model, CG=2):
        self.model = model
        # Namespace to hold all the temperature solving pieces
        self.temperature = SimpleNamespace()
        ds = self.temperature.ds = ufl.Measure(
            "ds", domain=self.model.msh, subdomain_data=self.model.facet_tag
        )

        V = self.temperature.V = FunctionSpace(self.model.msh, ("CG", CG))
        u = self.temperature.u = ufl.TrialFunction(V)
        v = self.temperature.v = ufl.TestFunction(V)
        I_HR = self.temperature.I_HR = Function(V)
        I_BR = self.temperature.I_BR = Function(V)
        I_TM_AR = self.temperature.I_TM_AR = Function(V)
        Q_SUB = self.temperature.Q_SUB = Function(V)

        self.temperature.boundary_conditions = [
            BoundaryCondition(
                "Robin",
                1,
                (self.model.eta / self.model.kappa, -I_HR / self.model.eta),
                u,
                v,
                ds,
            ),  # HR
            BoundaryCondition(
                "Robin",
                2,
                (self.model.eta / self.model.kappa, -I_TM_AR / self.model.eta),
                u,
                v,
                ds,
            ),  # AR
            BoundaryCondition(
                "Robin",
                3,
                (self.model.eta / self.model.kappa, -I_BR / self.model.eta),
                u,
                v,
                ds,
            ),  # Barrel
        ]

        self.temperature.F = (
            -ufl.inner(ufl.nabla_grad(u), ufl.nabla_grad(v))
        ) * ufl.dx + ufl.inner(Q_SUB, v) / self.model.kappa * ufl.dx

        for condition in self.temperature.boundary_conditions:
            self.temperature.F += condition.bc

        self.temperature.a = ufl.lhs(self.temperature.F)
        self.temperature.L = ufl.rhs(self.temperature.F)

        # Namespace to hold all the temperature solving pieces
        self.deformation = SimpleNamespace()
        self.deformation.V = get_function_space(self.model.msh, "CG", CG)
        self.deformation.u = ufl.TrialFunction(self.deformation.V)
        self.deformation.v = ufl.TestFunction(self.deformation.V)

        # Create the near-nullspace and attach it to the PETSc matrix:
        self.deformation.ns = build_nullspace(self.deformation.V)

    def solve_temperature(self, options={}):
        """Solve linear variational problem."""
        options = {"pc_type": "gamg", **options}  # overwrite if provided by users

        problem = LinearProblem(
            self.temperature.a, self.temperature.L, petsc_options=options
        )
        self.temperature.solution = problem.solve()
        return self.temperature.solution

    def solve_deformation(self, options={}):
        options = {
            "ksp_type": "cgs",
            "ksp_rtol": 1.0e-10,
            "pc_type": "gamg",
            "mg_levels_ksp_type": "chebyshev",
            "mg_levels_pc_type": "jacobi",
            "mg_levels_esteig_ksp_type": "cgs",
            "mg_levels_ksp_chebyshev_esteig_steps": 40,
            "mg_coarse_pc_type": "jacobi",  # jacobi coarse solver for singular problem
            **options,  # overwrites any defaults
        }

        self.deformation.F = (
            ufl.inner(
                self.model.sigma(self.deformation.u, self.temperature.solution),
                self.model.epsilon(self.deformation.v),
            )
            * ufl.dx
        )
        self.deformation.a = ufl.lhs(self.deformation.F)
        self.deformation.L = ufl.rhs(self.deformation.F)

        problem = LinearProblem(
            self.deformation.a, self.deformation.L, bcs=[], petsc_options=options
        )
        problem.A.setNullSpace(self.deformation.ns)
        problem.A.setNearNullSpace(self.deformation.ns)

        self.deformation.solution = problem.solve()
        return self.deformation.solution

    def evaluate_temperature(self, x, y, z, *, meshgrid=False):
        """Evaluates the temperature solution at various coordinates throughout the
        mesh.

        Parameters
        ----------
        x, y, z : arraylike
            If meshgrid is True, these should be 1D vectors describing the
            volume of points to evaluate the solution over. These inputs
            will be inputted to meshgrid to determine what points to evaluate.
            If meshgrid is False then x,y,z describe the array of points to
            evaluate at. If False x,y,z must be the same length or singular
            values.
        meshgrid : bool, optional
            Whether to use meshgrid to project the provided x,y,z values
            to a grid on which to evaluate

        Returns
        -------
            points : arraylike
                The x, y, z positions of evaluated points. If meshgrid is
                True the shape is (x.size, y.size, z.size, 3), otherwise
                it has a shape of (max(x.size, y.size, z.size), 1).
            result : arraylike
                The evaluated points [K]. If meshgrid is True the shape is
                (x.size, y.size, z.size, 3), otherwise it has a shape of
                (max(x.size, y.size, z.size), 1).
            mask : arraylike[bool]
                A boolean mask, False values are points which could not be
                evaluated, typically those outside of the mesh
        """
        return evaluate_solution(
            self.model.msh, x, y, z, self.temperature.solution, meshgrid=meshgrid
        )

    def evaluate_deformation(self, x, y, z, *, meshgrid=False):
        """Evaluates the thermo-elastic deformation solution at various coordinates
        throughout the mesh.

        Parameters
        ----------
        x, y, z : arraylike
            If meshgrid is True, these should be 1D vectors describing the
            volume of points to evaluate the solution over. These inputs
            will be inputted to meshgrid to determine what points to evaluate.
            If meshgrid is False then x,y,z describe the array of points to
            evaluate at. If False x,y,z must be the same length or singular
            values.
        meshgrid : bool, optional
            Whether to use meshgrid to project the provided x,y,z values
            to a grid on which to evaluate

        Returns
        -------
            points : arraylike
                The x, y, z positions of evaluated points. If meshgrid is
                True the shape is (x.size, y.size, z.size, 3), otherwise
                it has a shape of (max(x.size, y.size, z.size), 3).
            result : arraylike
                The evaluated points [m]. If meshgrid is True the shape is
                (x.size, y.size, z.size, 3), otherwise it has a shape of
                (max(x.size, y.size, z.size), 3).
            mask : arraylike[bool]
                A boolean mask, False values are points which could not be
                evaluated, typically those outside of the mesh
        """
        return evaluate_solution(
            self.model.msh, x, y, z, self.deformation.solution, meshgrid=meshgrid
        )


def make_test_mass_model(
    mesh_function=cylinder_3D, mesh_function_kwargs={}, model=None
):
    if model is None:
        model = AdvancedLIGOTestMass3D()
        model.radius = 0.17
        model.thickness = 0.2
    add_flats = False
    flat_hw = 0.3265 / 2
    if "add_flats" in mesh_function_kwargs:
        add_flats = mesh_function_kwargs["add_flats"]
        mesh_function_kwargs["flat_hw"] = 0.3265 / 2
    else:
        mesh_function_kwargs["add_flats"] = add_flats
        mesh_function_kwargs["flat_hw"] = 0.3265 / 2

    if "radius" in mesh_function_kwargs:
        model.radius = mesh_function_kwargs["radius"]
        mesh_function_kwargs.pop("radius")
    if "thickness" in mesh_function_kwargs:
        model.thickness = mesh_function_kwargs["thickness"]
        mesh_function_kwargs.pop("thickness")

    model.msh, model.cell_markers, model.facet_markers = mesh_function(
        model.radius, model.thickness, **mesh_function_kwargs
    )

    # Marking boundaries
    def boundary_HR(x):
        """Return geometry entities close to HR surface."""
        return np.isclose(x[2], model.thickness / 2)

    def boundary_AR(x):
        """Return geometry entities close to AR surface."""
        return np.isclose(x[2], -model.thickness / 2)

    def boundary_barrel(x):
        """Return geometry entities close to barrel surface."""
        if add_flats is False:
            bb = np.isclose(x[0] ** 2 + x[1] ** 2, model.radius**2)
        else:
            bb = np.bitwise_or(
                np.isclose(x[0] ** 2 + x[1] ** 2, model.radius**2),
                np.isclose(abs(x[0]), flat_hw),
            )
        return bb

    boundaries = [
        (1, boundary_HR),
        (2, boundary_AR),
        (3, boundary_barrel),
    ]
    # We now loop through all the boundary conditions and create MeshTags
    # identifying the facets for each boundary condition.
    model.facet_indices, model.facet_markers = [], []
    fdim = model.msh.topology.dim - 1  # Facet dimension (2)

    for marker, locator in boundaries:
        facets = locate_entities(model.msh, fdim, locator)
        # Find facets
        model.facet_indices.append(facets)
        # Label facet markers
        model.facet_markers.append(np.full_like(facets, marker))

    model.facet_indices = np.hstack(model.facet_indices).astype(np.int32)
    model.facet_markers = np.hstack(model.facet_markers).astype(np.int32)
    model.sorted_facets = np.argsort(model.facet_indices)

    model.facet_tag = meshtags(
        model.msh,
        fdim,
        model.facet_indices[model.sorted_facets],
        model.facet_markers[model.sorted_facets],
    )

    model.msh.topology.create_connectivity(
        model.msh.topology.dim - 1, model.msh.topology.dim
    )
    return model


def build_steady_state_solver(model):
    return AdvancedLIGOTestMass3DSteadyState(model)


class AdvancedLIGOTestMass3DTime:
    def __init__(self, model, dt):
        self.model = model
        # Define test function space:
        ds = ufl.Measure(
            "ds", domain=self.model.msh, subdomain_data=self.model.facet_tag
        )
        self.t = 0
        dt = self.dt = Constant(model.msh, PETSc.ScalarType(dt))
        # variable to hold exisiting dt value
        self.dt0 = self.dt.value.copy()

        self.temperature = SimpleNamespace()
        V = self.temperature.V = FunctionSpace(model.msh, ("CG", 2))
        u = self.temperature.u = ufl.TrialFunction(self.temperature.V)
        v = self.temperature.v = ufl.TestFunction(self.temperature.V)
        self.temperature.I_HR = Function(V)
        self.temperature.I_BR = Function(V)
        kappa = model.kappa
        rho = model.rho
        C = model.C
        f = self.f = Constant(model.msh, PETSc.ScalarType(0))  # internal source term?

        u_n = self.temperature.u_n = Function(self.temperature.V)
        self.temperature.u_n.name = "u_n"

        self.temperature.uh = Function(V)
        self.temperature.uh.name = "uh"

        F = -u * v * ufl.dx
        F += -(kappa / (rho * C)) * dt * ufl.dot(ufl.grad(u), ufl.grad(v)) * ufl.dx
        F += (u_n + (1 / (rho * C)) * dt * f) * v * ufl.dx

        boundary_conditions = {}
        boundary_conditions["HR"] = BoundaryCondition(
            "Robin",
            1,
            (
                self.model.eta / self.model.kappa,
                -self.temperature.I_HR / self.model.eta,
            ),
            u,
            v,
            ds,
        )
        boundary_conditions["AR"] = BoundaryCondition(
            "Robin", 2, (self.model.eta / self.model.kappa, 0), u, v, ds
        )
        boundary_conditions["BR"] = BoundaryCondition(
            "Robin",
            3,
            (
                self.model.eta / self.model.kappa,
                -self.temperature.I_BR / self.model.eta,
            ),
            u,
            v,
            ds,
        )

        for condition in boundary_conditions.values():
            F += (kappa / (rho * C)) * dt * condition.bc

        self.temperature.a = form(ufl.lhs(F))
        self.temperature.L = form(ufl.rhs(F))

        self.temperature.A = create_matrix(self.temperature.a)
        self.temperature.A.zeroEntries()
        assemble_matrix(self.temperature.A, self.temperature.a, bcs=[])
        self.temperature.A.assemble()
        self.temperature.b = create_vector(self.temperature.L)

        self.temperature.solver = PETSc.KSP().create(model.msh.comm)
        self.temperature.solver.setOperators(self.temperature.A)
        self.temperature.solver.setType(PETSc.KSP.Type.CG)
        self.temperature.solver.getPC().setType(PETSc.PC.Type.GAMG)

        # Namespace to hold all the deformation solving pieces
        self.deformation = SimpleNamespace()
        self.deformation.V = get_function_space(self.model.msh, "CG", 2)
        self.deformation.T = self.temperature.uh
        self.deformation.uh = Function(self.deformation.V)
        self.deformation.u = ufl.TrialFunction(self.deformation.V)
        self.deformation.v = ufl.TestFunction(self.deformation.V)
        # Create the near-nullspace and attach it to the PETSc matrix:
        self.deformation.ns = build_nullspace(self.deformation.V)
        self.deformation.F = (
            ufl.inner(
                self.model.sigma(self.deformation.u, self.deformation.T),
                self.model.epsilon(self.deformation.v),
            )
            * ufl.dx
        )
        self.deformation.a = form(ufl.lhs(self.deformation.F))
        self.deformation.L = form(ufl.rhs(self.deformation.F))

        self.deformation.A = assemble_matrix(self.deformation.a)
        self.deformation.A.assemble()
        self.deformation.A.setNullSpace(self.deformation.ns)
        self.deformation.A.setNearNullSpace(self.deformation.ns)
        self.deformation.b = create_vector(self.deformation.L)

        self.deformation.solver = PETSc.KSP().create(model.msh.comm)
        self.deformation.solver.setOperators(self.deformation.A)
        self.deformation.solver.setType(PETSc.KSP.Type.CG)
        self.deformation.solver.getPC().setType(PETSc.PC.Type.GAMG)

    def set_initial_condition(self, condition):
        self.temperature.u_n.interpolate(condition)
        self.temperature.uh.interpolate(condition)

    def step(self):
        self.t += self.dt.value
        if self.dt0 != self.dt.value:
            self.temperature.A.zeroEntries()
            assemble_matrix(self.temperature.A, self.temperature.a, bcs=[])
            self.temperature.A.assemble()
            self.dt0 = self.dt.value.copy()

        def solve(state, uh, u_n):
            # Update the right hand side reusing the initial vector
            with state.b.localForm() as loc_b:
                loc_b.set(0)
            assemble_vector(state.b, state.L)
            state.solver.solve(state.b, state.uh.vector)
            uh.x.scatter_forward()
            if u_n is not None:
                # Update solution at previous time step (u_n)
                u_n.x.array[:] = uh.x.array

        solve(self.temperature, self.temperature.uh, self.temperature.u_n)
        solve(self.deformation, self.deformation.uh, None)

    def evaluate_temperature(self, x, y, z, *, meshgrid=False):
        """Evaluates the temperature solution at various coordinates throughout the
        mesh.

        Parameters
        ----------
        x, y, z : arraylike
            If meshgrid is True, these should be 1D vectors describing the
            volume of points to evaluate the solution over. These inputs
            will be inputted to meshgrid to determine what points to evaluate.
            If meshgrid is False then x,y,z describe the array of points to
            evaluate at. If False x,y,z must be the same length or singular
            values.
        meshgrid : bool, optional
            Whether to use meshgrid to project the provided x,y,z values
            to a grid on which to evaluate

        Returns
        -------
            points : arraylike
                The x, y, z positions of evaluated points. If meshgrid is
                True the shape is (x.size, y.size, z.size, 3), otherwise
                it has a shape of (max(x.size, y.size, z.size), 1).
            result : arraylike
                The evaluated points [K]. If meshgrid is True the shape is
                (x.size, y.size, z.size, 3), otherwise it has a shape of
                (max(x.size, y.size, z.size), 1).
            mask : arraylike[bool]
                A boolean mask, False values are points which could not be
                evaluated, typically those outside of the mesh
        """
        return evaluate_solution(
            self.model.msh, x, y, z, self.temperature.uh, meshgrid=meshgrid
        )

    def evaluate_deformation(self, x, y, z, *, meshgrid=False):
        """Evaluates the thermo-elastic deformation solution at various coordinates
        throughout the mesh.

        Parameters
        ----------
        x, y, z : arraylike
            If meshgrid is True, these should be 1D vectors describing the
            volume of points to evaluate the solution over. These inputs
            will be inputted to meshgrid to determine what points to evaluate.
            If meshgrid is False then x,y,z describe the array of points to
            evaluate at. If False x,y,z must be the same length or singular
            values.
        meshgrid : bool, optional
            Whether to use meshgrid to project the provided x,y,z values
            to a grid on which to evaluate

        Returns
        -------
            points : arraylike
                The x, y, z positions of evaluated points. If meshgrid is
                True the shape is (x.size, y.size, z.size, 3), otherwise
                it has a shape of (max(x.size, y.size, z.size), 3).
            result : arraylike
                The evaluated points [m]. If meshgrid is True the shape is
                (x.size, y.size, z.size, 3), otherwise it has a shape of
                (max(x.size, y.size, z.size), 3).
            mask : arraylike[bool]
                A boolean mask, False values are points which could not be
                evaluated, typically those outside of the mesh
        """
        return evaluate_solution(
            self.model.msh, x, y, z, self.deformation.uh, meshgrid=meshgrid
        )
