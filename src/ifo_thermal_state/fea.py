import ufl
import numpy as np
import dolfinx
from petsc4py import PETSc
from dolfinx import la

if dolfinx.__version__ < "0.7":
    from dolfinx.geometry import (
        BoundingBoxTree,
        compute_collisions,
        compute_colliding_cells,
    )

    def evaluate_solution(mesh, x, y, z, U, meshgrid):
        return evaluate_solution_v6(mesh, x, y, z, U, meshgrid)

    def build_nullspace(V):
        return build_nullspace_v6(V)

else:
    from dolfinx.geometry import (
        compute_collisions_points,
        compute_colliding_cells,
        bb_tree,
    )

    def evaluate_solution(mesh, x, y, z, U, meshgrid):
        return evaluate_solution_v7(mesh, x, y, z, U, meshgrid)

    def build_nullspace(V):
        return build_nullspace_v7(V)


class BoundaryCondition:
    def __init__(self, type, marker, values, u, v, ds):
        self._type = type
        if type == "Neumann":
            self._bc = ufl.inner(values, v) * ds(marker)
        elif type == "Robin":
            _r, _s = values
            self._bc = _r * ufl.inner(u - _s, v) * ds(marker)
        else:
            raise TypeError("Unknown boundary condition: {0:s}".format(type))

    @property
    def bc(self):
        return self._bc

    @property
    def type(self):
        return self._type


def evaluate_solution_v6(mesh, x, y, z, U, meshgrid):
    x = np.atleast_1d(x)
    y = np.atleast_1d(y)
    z = np.atleast_1d(z)

    if meshgrid:
        X, Y, Z = np.meshgrid(x, y, z)
        N = X.size
    else:
        X, Y, Z = x, y, z
        N = np.max((X.size, Y.size, Z.size))

    result = np.empty((N, 3))
    result[:] = np.nan
    mask = np.zeros(N, dtype=bool)

    points = np.zeros((N, 3))
    points[:, 0] = X.flat
    points[:, 1] = Y.flat
    points[:, 2] = Z.flat

    bb_tree = BoundingBoxTree(mesh, mesh.topology.dim)

    cells = []

    # Find cells whose bounding-box collide with the the points
    cell_candidates = compute_collisions(bb_tree, points)
    # choose one of the cells that contains the point
    colliding_cells = compute_colliding_cells(mesh, cell_candidates, points)

    for i in range(len(points)):
        if len(colliding_cells.links(i)) > 0:
            mask[i] = True
            cells.append(colliding_cells.links(i)[0])

    result[mask] = U.eval(points[mask], cells)

    if meshgrid:
        return (
            points.reshape(x.size, y.size, z.size, 3),
            result.reshape(x.size, y.size, z.size, 3),
            mask.reshape(x.size, y.size, z.size),
        )
    else:
        return (
            points,
            result,
            mask,
        )


def evaluate_solution_v7(mesh, x, y, z, U, meshgrid):
    x = np.atleast_1d(x)
    y = np.atleast_1d(y)
    z = np.atleast_1d(z)

    if meshgrid:
        X, Y, Z = np.meshgrid(x, y, z)
        N = X.size
    else:
        X, Y, Z = x, y, z
        N = np.max((X.size, Y.size, Z.size))

    result = np.empty((N, 3))
    result[:] = np.nan
    mask = np.zeros(N, dtype=bool)

    points = np.zeros((N, 3))
    points[:, 0] = X.flat
    points[:, 1] = Y.flat
    points[:, 2] = Z.flat

    bb_tree_model = bb_tree(mesh, mesh.topology.dim)

    cells = []

    # Find cells whose bounding-box collide with the the points
    cell_candidates = compute_collisions_points(bb_tree_model, points)
    # choose one of the cells that contains the point
    colliding_cells = compute_colliding_cells(mesh, cell_candidates, points)

    for i in range(len(points)):
        if len(colliding_cells.links(i)) > 0:
            mask[i] = True
            cells.append(colliding_cells.links(i)[0])

    result[mask] = U.eval(points[mask], cells)

    if meshgrid:
        return (
            points.reshape(x.size, y.size, z.size, 3),
            result.reshape(x.size, y.size, z.size, 3),
            mask.reshape(x.size, y.size, z.size),
        )
    else:
        return (
            points,
            result,
            mask,
        )


def build_nullspace_v6(V):
    """Build PETSc nullspace for 3D elasticity."""

    # Create vectors that will span the nullspace
    bs = V.dofmap.index_map_bs
    length0 = V.dofmap.index_map.size_local
    length1 = length0 + V.dofmap.index_map.num_ghosts
    basis = [np.zeros(bs * length1, dtype=PETSc.ScalarType) for i in range(6)]

    # Get dof indices for each subspace (x, y and z dofs)
    dofs = [V.sub(i).dofmap.list.array for i in range(3)]

    # Set the three translational rigid body modes
    for i in range(3):
        basis[i][dofs[i]] = 1.0

    # Set the three rotational rigid body modes
    x = V.tabulate_dof_coordinates()
    dofs_block = V.dofmap.list.array
    x0, x1, x2 = x[dofs_block, 0], x[dofs_block, 1], x[dofs_block, 2]
    basis[3][dofs[0]] = -x1
    basis[3][dofs[1]] = x0
    basis[4][dofs[0]] = x2
    basis[4][dofs[2]] = -x0
    basis[5][dofs[2]] = x1
    basis[5][dofs[1]] = -x2

    # Create PETSc Vec objects (excluding ghosts) and normalise
    basis_petsc = [
        PETSc.Vec().createWithArray(x[: bs * length0], bsize=3, comm=V.mesh.comm)
        for x in basis
    ]
    la.orthonormalize(basis_petsc)
    assert la.is_orthonormal(basis_petsc)

    # Create and return a PETSc nullspace
    return PETSc.NullSpace().create(vectors=basis_petsc)


def build_nullspace_v7(V):
    """Build PETSc nullspace for 3D elasticity."""

    # Create vectors that will span the nullspace
    bs = V.dofmap.index_map_bs
    length0 = V.dofmap.index_map.size_local
    length1 = length0 + V.dofmap.index_map.num_ghosts
    basis = [np.zeros(bs * length1, dtype=PETSc.ScalarType) for i in range(6)]

    # Get dof indices for each subspace (x, y and z dofs)
    dofs = [V.sub(i).dofmap.list for i in range(3)]

    # Set the three translational rigid body modes
    for i in range(3):
        basis[i][dofs[i]] = 1.0

    # Set the three rotational rigid body modes
    x = V.tabulate_dof_coordinates()
    dofs_block = V.dofmap.list
    x0, x1, x2 = x[dofs_block, 0], x[dofs_block, 1], x[dofs_block, 2]
    basis[3][dofs[0]] = -x1
    basis[3][dofs[1]] = x0
    basis[4][dofs[0]] = x2
    basis[4][dofs[2]] = -x0
    basis[5][dofs[2]] = x1
    basis[5][dofs[1]] = -x2

    # Create PETSc Vec objects (excluding ghosts) and normalise
    basis_petsc = [
        PETSc.Vec().createWithArray(x[: bs * length0], bsize=3, comm=V.mesh.comm)
        for x in basis
    ]
    la.orthonormalize(basis_petsc)
    assert la.is_orthonormal(basis_petsc)

    # Create and return a PETSc nullspace
    return PETSc.NullSpace().create(vectors=basis_petsc)
