# %%
from ifo_thermal_state.aligo_3D import (
    make_test_mass_model,
    AdvancedLIGOTestMass3DSteadyState,
)
from ifo_thermal_state.mesh import sweep_cylinder_3D
from ifo_thermal_state.plotting import plot_mesh, plot_temperature
from ifo_thermal_state.math import composite_newton_cotes_weights
import matplotlib.pyplot as plt
import numpy as np
from types import SimpleNamespace
from scipy.interpolate import interp1d

# %%
z_RH = 30e-3
w_RH = 20e-3
H = 0.2
heights = [(z_RH - w_RH / 2) / H, (z_RH + (w_RH / 2)) / H, 1]
model = make_test_mass_model(
    mesh_function=sweep_cylinder_3D,
    mesh_function_kwargs={
        "num_elements": [3, 5, 15],
        "heights": heights,
        "add_flats": True,
        "HR_mesh_size": 0.02,
        "AR_mesh_size": 0.02,
        "mesh_algorithm": 6,
    },
)
plot_mesh(model.msh, model.cell_markers)

# %%
values = SimpleNamespace()
values.w = 53e-3
values.dx = 0
values.dy = 0
values.P_coat = 1

# Loading in ring heater realistic angular distribution
I_RH_theta = np.loadtxt("./RH_heatdistn.txt", delimiter=",")
theta_RH, _idx = np.unique(I_RH_theta[:, 0], return_index=True)
I_RH_data = I_RH_theta[:, 1][_idx]
plt.plot(theta_RH, I_RH_data)
I_RH_theta_interp = interp1d(
    theta_RH, I_RH_data, kind="linear", fill_value="extrapolate"
)

A_RH = 2 * np.pi * model.radius * w_RH


def I_RH(x):
    return (
        I_RH_theta_interp(np.arctan2(x[1], x[0]) + np.pi)
        * np.double(
            (x[2] >= -model.thickness / 2 + z_RH - w_RH / 2)
            & (x[2] <= -model.thickness / 2 + z_RH + w_RH / 2)
        )
        / A_RH
    )
    # return np.double((x[2] >= -model.thickness/2 + z_RH-w_RH/2) &
    #                   (x[2] <= -model.thickness/2 + z_RH+w_RH/2))/A_RH


ss = AdvancedLIGOTestMass3DSteadyState(model)


# %%
values.dx = 0e-3
ss.temperature.I_BR.interpolate(I_RH)
ss.solve_temperature()
ss.solve_deformation()

# %%
x, y, z = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
    [-0.1, 0.1],
)
# %%
xyz, W, mask = ss.evaluate_temperature(x, y, z, meshgrid=True)
# %%
fig, axs = plt.subplots(1, 2, figsize=(10, 4))

for i, title in zip(range(2), ["AR", "HR"]):
    _mask = mask[:, :, i]
    plt.sca(axs[i])
    plt.contourf(
        x,
        y,
        W[:, :, i, 0],
        20,
    )
    plt.gca().set_aspect("equal")
    cb0 = plt.colorbar()
    cb0.ax.set_ylabel("Temperature [K]")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.title(title)

plt.tight_layout()

# %%
# Plotting temperature field
plot_temperature(ss.temperature.V, ss.temperature.solution)
# %%

# Evaulate surface deformation
xyz, W, mask = ss.evaluate_deformation(x, y, z, meshgrid=True)


# %%
fig, axs = plt.subplots(1, 2, figsize=(10, 4))

for i, title in zip(range(2), ["AR", "HR"]):
    _mask = mask[:, :, i]
    plt.sca(axs[i])
    plt.contourf(
        x,
        y,
        1e9 * (W[:, :, i, 2] - W[_mask, i, 2].min()),
        20,
    )
    plt.gca().set_aspect("equal")
    cb0 = plt.colorbar()
    cb0.ax.set_ylabel("Surface deformation [nm]")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.title(title)

plt.tight_layout()
# %%
# OPD
x, y, z = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.1, 0.1, 5),
)
xyz, dT, mask = ss.evaluate_temperature(x, y, z, meshgrid=True)
W = composite_newton_cotes_weights(z.size, 5)
dz = z[1] - z[0]
OPD = (
    model.dndT
    * dz
    * np.sum(dT[:, :, :, 0] * W[None, None, :], axis=2)  # weight Z direction and sum
)
# %%
plt.contourf(
    x,
    y,
    OPD / 1e-6,
    20,
)
plt.gca().set_aspect("equal")
cb0 = plt.colorbar()
cb0.ax.set_ylabel("Optical path depth [um]")
plt.xlabel("x [m]")
plt.ylabel("y [m]")
plt.title(title)
# %%
