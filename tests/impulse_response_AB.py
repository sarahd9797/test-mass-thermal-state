# %%
from ifo_thermal_state.aligo_3D import (
    make_test_mass_model,
    AdvancedLIGOTestMass3DTime,
)

from ifo_thermal_state.postprocessing import get_deformation, get_opd
import numpy as np
import finesse
import finesse.ligo
from types import SimpleNamespace
from finesse.cymath.homs import HGModes
from copy import deepcopy
import time

finesse.init_plotting()

# %%
model = make_test_mass_model(
    mesh_function_kwargs={
        "HR_mesh_size": 0.03,
        "AR_mesh_size": 0.03,
        "mesh_algorithm": 6,
        "point": None,
    }
)


# %%
def run_full(plan, HOM, w=53e-3, xyc=[0.0, 0.0], dT=60, N=100, t_end=6000):
    x, y = (
        np.linspace(-0.17, 0.17, N),
        np.linspace(-0.17, 0.17, N),
    )

    values = SimpleNamespace()
    values.w = w
    values.homs = np.array(HOM, dtype=np.int32)
    values.E = np.zeros(values.homs.shape[0])
    values.E[0] = 1
    values.P = 1
    values.x0 = xyc[0]
    values.y0 = xyc[1]

    def I_ITM_HR(x):
        HGs = HGModes(finesse.BeamParam(w=values.w, Rc=np.inf), values.homs)
        a = HGs.compute_points(x[0] - values.x0, x[1] - values.y0) * values.E[:, None]
        E = np.sum(a, axis=0)
        I = values.P * E * E.conj()
        return I.real

    def initial_condition(x):
        return np.full((x.shape[1],), 0)

    tsim = AdvancedLIGOTestMass3DTime(model, dT)
    tsim.set_initial_condition(initial_condition)

    result = SimpleNamespace()
    result.x = x
    result.y = y
    result.t = [0]
    result.step = 0
    result.OPDs = []
    result.z_HR = []
    result.w = w
    result.HOM = HOM
    result.dT = dT
    result.N = N
    result.values = []

    result.OPDs.append(get_opd(x, y, tsim))
    result.z_HR.append(get_deformation(x, y, tsim))

    while tsim.t <= t_end:
        plan(result.t[-1], result.step, values)
        print(result.t[-1], end=f"/{t_end}\r")
        tsim.temperature.I_HR.interpolate(I_ITM_HR)
        result.values.append(deepcopy(values))
        tsim.step()
        result.t.append(tsim.t)
        result.OPDs.append(get_opd(x, y, tsim))
        result.z_HR.append(get_deformation(x, y, tsim))
        result.step += 1

    return result


def make_pulse_response(HOM, w=53e-3, xyc=[0.0, 0.0], dT=60, N=100, t_end=6000):
    def plan(t, step, values):
        if step == 0:
            values.P = 1
        else:
            values.P = 0

    return run_full(plan, HOM, w, xyc, dT, N, t_end)


# %%
start_time = time.time()
HOM = [[0, 1]]  # HG12
dT = 30
theta = [55e-3, 0.01, -0.01]
pulse = make_pulse_response(
    HOM, w=theta[0], xyc=[theta[1], theta[2]], t_end=2 * 3600, dT=dT
)
x = pulse.x
y = pulse.y
t = pulse.t
OPD_nm = np.array(pulse.OPDs) / 1e-9  # in nanometers
z_HR_nm = np.array(pulse.z_HR) / 1e-9  # in nanometers
print(time.time() - start_time)
np.save("OPD_nm", OPD_nm)
np.save("z_HR_nm", z_HR_nm)
np.save("x", x)
np.save("y", y)
np.save("t", t)
# # %%

# #pulse = pulse_w45
# plt.contourf(pulse.t, pulse.x, np.array(pulse.OPDs)[:, :, 50].T / 1e-9)
# plt.xlabel("Time [s]")
# plt.ylabel("x [m]")
# plt.colorbar(label="OPD [nm]")
# plt.title(f"Pulse response w={pulse.w/1e-3:.0f} [mm]")


# # %%
# def current_OPD(x, y, pulses, step=-1):
#     """Computes the OPD at a particular step in a series of pulses."""
#     if len(pulses) == 0:
#         return np.zeros((x.size, y.size), dtype=float)

#     current_step = pulses[step].step
#     OPD = np.zeros((x.size, y.size), dtype=float)

#     for pulse in pulses:
#         OPD += pulse.P * pulse.func.OPDs[int(current_step - pulse.step)]

#     return OPD


# # %% Steady state example
# steady_state_pulses = []  # pulses being applied
# pulse_sum_OPD = []  # Time varying OPD from separate pulse calculation
# pulse_sum_OPD.append(current_OPD(x, y, steady_state_pulses))  # initial zero

# # Generate the pulse train and store them all in a list
# # afterwards this list can be iterated over and the OPD
# # solved at some time point computed
# for i in range(6000 // 60 + 1):
#     t = i * dT
#     pulse = SimpleNamespace()
#     pulse.w = 55e-3
#     pulse.dT = dT
#     pulse.P = 1
#     pulse.step = i
#     pulse.t = t
#     pulse.func = pulse_w55
#     steady_state_pulses.append(pulse)
#     # Calculate current OPD as we go
#     pulse_sum_OPD.append(current_OPD(x, y, steady_state_pulses))


# # %% Compute the "real" result
# def plan(t, step, values):
#     values.P = 1


# real_result = run_full(plan, HOM, w=pulse_w55.w, dT=60, N=100, t_end=6000)

# # %%
# plt.title("Final error")
# plt.imshow(np.abs(real_result.OPDs[-1] - pulse_sum_OPD[-1]) / abs(real_result.OPDs[-1]))
# plt.colorbar()

# # %%
# pulse = real_result
# plt.contourf(pulse.t, pulse.x, np.array(pulse.OPDs)[:, :, 50].T / 1e-9)
# plt.xlabel("Time [s]")
# plt.ylabel("x [m]")
# plt.colorbar(label="OPD [nm]")
# plt.title(f"Real steady state response w={pulse.w/1e-3:.0f} [mm]")

# # %%
# plt.contourf(pulse.t, pulse.x, np.array(pulse_sum_OPD)[:, :, 50].T / 1e-9)
# plt.xlabel("Time [s]")
# plt.ylabel("x [m]")
# plt.colorbar(label="OPD [nm]")
# plt.title(f"Summed pulse steady state response w={pulse.w/1e-3:.0f} [mm]")


# # %% Spot change example, increases from 45 to 55mm 50 steps in,  power also changes
# spot_change_pulses = []  # pulses being applied
# spot_change_pulse_sum_OPD = []  # Time varying OPD from separate pulse calculation
# spot_change_pulse_sum_OPD.append(current_OPD(x, y, spot_change_pulses))  # initial zero

# for i in range(6000 // 60 + 1):
#     t = i * dT
#     pulse = SimpleNamespace()
#     pulse.dT = dT
#     pulse.P = 1
#     pulse.step = i
#     pulse.t = t
#     if pulse.step > 50:
#         pulse.w = 55e-3
#         pulse.func = pulse_w55
#         pulse.P = 0.5
#     else:
#         pulse.w = 45e-3
#         pulse.func = pulse_w45
#         pulse.P = 2
#     spot_change_pulses.append(pulse)
#     # Calculate current OPD as we go
#     spot_change_pulse_sum_OPD.append(current_OPD(x, y, spot_change_pulses))


# # %% Compute the "real" result
# def plan(t, step, values):
#     values.P = 1
#     if step > 50:
#         values.w = 55e-3
#         values.P = 0.5
#     else:
#         values.w = 45e-3
#         values.P = 2


# real_spot_change_result = run_full(plan, HOM, w=pulse_w45.w, dT=60, N=100, t_end=6000)

# # %%
# pulse = real_spot_change_result
# plt.contourf(pulse.t, pulse.x, np.array(pulse.OPDs)[:, :, 50].T / 1e-9, levels=20)
# plt.xlabel("Time [s]")
# plt.ylabel("x [m]")
# plt.colorbar(label="OPD [nm]")
# plt.title("Real spot and power changing")

# # %%
# plt.contourf(
#     pulse.t, pulse.x, np.array(spot_change_pulse_sum_OPD)[:, :, 50].T / 1e-9, levels=20
# )
# plt.xlabel("Time [s]")
# plt.ylabel("x [m]")
# plt.colorbar(label="OPD [nm]")
# plt.title("Summed pulse spot and power changing")

# # %%
# plt.contourf(
#     pulse.t,
#     pulse.x,
#     np.array(pulse.OPDs)[:, :, 50].T / 1e-9
#     - np.array(spot_change_pulse_sum_OPD)[:, :, 50].T / 1e-9,
#     levels=20,
# )
# plt.xlabel("Time [s]")
# plt.ylabel("x [m]")
# plt.colorbar(label="OPD [nm]")
# plt.title("Error:: summed pulse spot and power changing")

# # %%
# # plt.title("Final error")
# # plt.imshow(
# #     np.abs(real_spot_change_result.OPDs[-1] - spot_change_pulse_sum_OPD[-1])
# #     / abs(real_spot_change_result.OPDs[-1])
# # )
# # plt.colorbar()
# # %%
