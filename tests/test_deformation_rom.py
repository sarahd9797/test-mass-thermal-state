import tensorflow as tf
import numpy as np
import h5py
import pylab as plt

ml_fits_AR = tf.keras.models.load_model("../src/roms/tf_AR_defo")
ml_fits_HR = tf.keras.models.load_model("../src/roms/tf_HR_defo")

rombus_model_AR = h5py.File(
    "..//batch_runs/model/deformation_rom/model_AR_from_h5.hdf5",
    "r",
)

rombus_model_HR = h5py.File(
    "..//batch_runs/model/deformation_rom/model_HR_from_h5.hdf5",
    "r",
)


B_matrix_AR = rombus_model_AR.get("upsampled_interpolant/B_matrix")[:]
B_matrix_HR = rombus_model_HR.get("upsampled_interpolant/B_matrix")[:]

dx_min, dx_max = -40e-3, 40e-3
dy_min, dy_max = -40e-3, 40e-3
w_min, w_max = 45e-3, 70e-3

dx = np.random.uniform(low=dx_min, high=dx_max)
dy = np.random.uniform(low=dy_min, high=dy_max)
w = np.random.uniform(low=w_min, high=w_max)

params = np.array([dx, dy, w])

# NOTE: the normalization is hardcoded at the moment
# TODO: package tf model so that this data is easy to keep track of
EIM_coefficients_AR = ml_fits_AR.predict(np.array([params]))[0] * (
    -3.9309979648961396e-08
)
EIM_coefficients_HR = ml_fits_HR.predict(np.array([params]))[0] * 3.9339809681783035e-09

ROM_AR = np.dot(EIM_coefficients_AR, B_matrix_AR).reshape(200, 200)
ROM_HR = np.dot(EIM_coefficients_HR, B_matrix_HR).reshape(200, 200)

x, y, z = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.1, 0.1, 5),
)
# %%

plt.contourf(
    x,
    y,
    ROM_AR * 1e9,
    20,
)
plt.gca().set_aspect("equal")
cb0 = plt.colorbar()
cb0.ax.set_ylabel("surface deformation (AR) [nm]")
plt.xlabel("x [m]")
plt.ylabel("y [m]")
plt.title("ROM")
plt.savefig("deformation_AR_ROM.pdf", bbox_inches="tight")
plt.clf()

plt.contourf(
    x,
    y,
    ROM_HR * 1e9,
    20,
)
plt.gca().set_aspect("equal")
cb0 = plt.colorbar()
cb0.ax.set_ylabel("surface deformation (HR) [nm]")
plt.xlabel("x [m]")
plt.ylabel("y [m]")
plt.title("ROM")
plt.savefig("deformation_HR_ROM.pdf", bbox_inches="tight")
