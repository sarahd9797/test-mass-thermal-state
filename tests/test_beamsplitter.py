# %%
from ifo_thermal_state.ce_3D import make_beamsplitter_model, CE3DBeamSplitterSteadyState
import matplotlib.pyplot as plt
import numpy as np
from types import SimpleNamespace
import finesse
from dolfinx.fem import form, Function
from dolfinx.fem.petsc import (
    create_vector,
    create_matrix,
    assemble_matrix,
    assemble_vector,
)
from petsc4py import PETSc
from ifo_thermal_state.plotting import plot_temperature

finesse.init_plotting()


# %%
r_bs = 0.3
h_bs = 0.0845
# Build mesh for beam splitter
model = make_beamsplitter_model(
    radius=r_bs,
    thickness=h_bs,
    mesh_function_kwargs={
        "BS_mesh_size": 0.02,
        "AR_mesh_size": 0.02,
        "mesh_algorithm": 6,
    },
)


# %%
material_values = SimpleNamespace()
material_values.n = 1.4496
wavelen = 1064e-9
k = 2 * np.pi / wavelen


def bs_gaussian_mode(w0, aoi, bs_thickness, bs_radius, P_BS=1, P_AR=1, N=201):
    """Return a gaussian mode intensities on a beam splitter incident at aoi with some
    bs_thickness:

    Parameters:
    ----------------------------
    w0  : float
        beam-size radius incident  on bs
    aoi : float
        bs angle of incidence
    bs_thickness: float
        bs thickness
    bs_radius: float
        radius of bs
    P_BS: float
        absorbed power on BS side, default: 1
    P_AR: float
        absorbed power on AR side, default: 1
    N:  int
        Sampling

    Returns:
    ------------------------------
    x, y: array-like
        1-D coordinates
    U_BS, U_AR: array-like
        Field ar BS and AR surfaces
    """
    # Snell's law for the angle inside the beamsplitter:
    theta_trans = np.arcsin(np.sin(np.rad2deg(aoi)) / np.sin(material_values.n))
    x_inc = -bs_thickness * np.tan(theta_trans) / 2
    x_min = np.min([x_inc - 4 * w0, -1.2 * bs_radius])
    x = np.linspace(x_min, -x_min, N)
    y = np.linspace(x_min, -x_min, N)
    _xx, _yy = np.meshgrid(x, y)
    _xx1_BS = _xx * np.cos(aoi) - x_inc
    _xx1_AR = _xx * np.cos(aoi) + x_inc
    _yy1 = _yy
    _zz1 = -_xx * np.sin(aoi)
    # Rayleigh range:
    zR = np.pi * w0**2 / wavelen
    # ROC:
    inv_Rz = _zz1 / (_zz1**2 + zR**2)
    E0_BS = np.sqrt(2 * P_BS * np.cos(aoi) / (np.pi * w0**2))
    E0_AR = np.sqrt(2 * P_AR * np.cos(aoi) / (np.pi * w0**2))
    wz = w0 * np.sqrt(1 + _zz1 / zR)
    U_BS = (
        E0_BS
        * w0
        / wz
        * np.exp(-(_xx1_BS**2 + _yy**2) / wz**2)
        * np.exp(
            -1j
            * (
                (k * _zz1)
                + k * (_xx1_BS**2 + _yy1**2) * inv_Rz / 2
                - np.arctan(_zz1 / zR)
            )
        )
    )
    U_AR = (
        E0_AR
        * w0
        / wz
        * np.exp(-(_xx1_AR**2 + _yy**2) / wz**2)
        * np.exp(
            -1j
            * (
                (k * _zz1)
                + k * (_xx1_AR**2 + _yy1**2) * inv_Rz / 2
                - np.arctan(_zz1 / zR)
            )
        )
    )
    return x, y, U_BS, U_AR


# Compute clipping loss:
def bs_clipping_loss(x, y, U, r):
    dx = x[1] - x[0]
    dy = y[1] - y[0]
    _xx, _yy = np.meshgrid(x, y)
    M = np.ones_like(_xx)
    M[_xx**2 + _yy**2 > r**2] = 0
    clip_loss = 1 - (M * np.real(U * np.conj(U)) * dx * dy).sum()
    return clip_loss


# qBS = BeamParam(wavelength =  1064e-9, w0 = 106e-3, z = 0)
xBS, yBS, U_BS, U_AR = bs_gaussian_mode(40.5e-3, 45, h_bs, r_bs, N=501)
I_BS = np.real(U_BS * np.conj(U_BS))
I_AR = np.real(U_AR * np.conj(U_AR))
print(I_BS.sum() * (xBS[1] - xBS[0]) ** 2)

fig, axs = plt.subplots(1, 2, figsize=(8, 4))
axs[0].contourf(xBS, yBS, I_BS, 20)
axs[0].set_aspect("equal")
bs_outline = plt.Circle((0, 0), r_bs, color="r", fill=False)
ar_outline = plt.Circle((0, 0), r_bs, color="r", fill=False)

axs[0].add_patch(bs_outline)
axs[1].contourf(xBS, yBS, I_AR, 20)
axs[1].set_aspect("equal")
axs[1].add_patch(ar_outline)

for ax in axs:
    ax.set_xlabel("x [m]")
    ax.set_ylabel("y [m]")
axs[0].set_title("BS")
axs[1].set_title("AR")
plt.tight_layout()

# Clipping loss:
U_BS_loss = bs_clipping_loss(xBS, yBS, U_BS, r_bs)
U_AR_loss = bs_clipping_loss(xBS, yBS, U_AR, r_bs)
print(f"BS-surface clipping loss at : {(1e6*U_BS_loss):.2f} ppm")
print(f"AR-surface clipping loss: {(1e6*U_AR_loss):.2f} ppm")


# %%


def get_transmitted_angle(aoi):
    return np.arcsin(1 * np.sin(aoi) / material_values.n)


def get_AR_position(x0_BS, aoi, thickness):
    theta_t = np.arcsin(1 * np.sin(aoi) / material_values.n)
    x0_AR = np.tan(theta_t) * thickness - x0_BS
    return x0_AR


w0 = 50e-3
x0_BS = 0.0  # Incident position
theta_i = np.deg2rad(45)
P_BS = 1.0
P_AR = 1.0
theta_t = np.cos(theta_i)
x0_AR = get_AR_position(x0_BS, theta_i, h_bs)
x0_midBS = x0_AR / 2


def I_BS(x):
    I0 = 2 * P_BS * np.cos(theta_i) / (np.pi * w0**2)
    x_BS = x[0] * np.cos(theta_i) - x0_BS
    return I0 * np.exp(-(x_BS**2 + x[1] ** 2) / w0**2)


def I_AR(x):
    I0 = 2 * P_AR * np.cos(theta_i) / (np.pi * w0**2)
    x_AR = x[0] * np.cos(theta_i) - x0_AR
    return I0 * np.exp(-(x_AR**2 + x[1] ** 2) / w0**2)


def Q_SUB(x):
    theta_t = get_transmitted_angle(theta_i)
    x_beam = (x[0] - x0_midBS) * np.cos(-theta_t)
    I0 = 2 * P_BS / (np.pi * w0**2)
    return I0 * np.exp(-(x_beam**2 + x[1] ** 2) / w0**2) / h_bs


ss = CE3DBeamSplitterSteadyState(model)

ss.temperature.I_BS.interpolate(I_BS)
ss.temperature.I_AR.interpolate(I_AR)
ss.temperature.Q_SUB.interpolate(Q_SUB)

# %%
#  Create matrix + vector to run parametric study
_a_temperature = form(ss.temperature.a)
_L_temperature = form(ss.temperature.L)
A_temperature = create_matrix(_a_temperature)
b_temperature = create_vector(_L_temperature)

solver = PETSc.KSP().create(model.msh.comm)
solver.setOperators(A_temperature)
solver.setType(PETSc.KSP.Type.CG)
solver.getPC().setType(PETSc.PC.Type.HYPRE)
solver.getPC().setHYPREType("boomeramg")

uh = Function(ss.temperature.V)
assemble_matrix(A_temperature, _a_temperature, bcs=[])
A_temperature.assemble()
assemble_vector(b_temperature, _L_temperature)

solver.solve(b_temperature, uh.vector)

# %%
plot_temperature(ss.temperature.V, uh, mesh_edge=False, clip=True)
# %%
