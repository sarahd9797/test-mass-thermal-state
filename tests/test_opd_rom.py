import tensorflow as tf
import numpy as np
import h5py
import pylab as plt

ml_fits = tf.keras.models.load_model("../src/roms/tf_opd")

rombus_model = h5py.File(
    "..//batch_runs/model/opd_rom/model_from_h5.hdf5",
    "r",
)
B_matrix = rombus_model.get("upsampled_interpolant/B_matrix")[:]

dx_min, dx_max = -40e-3, 40e-3
dy_min, dy_max = -40e-3, 40e-3
w_min, w_max = 45e-3, 70e-3

dx = np.random.uniform(low=dx_min, high=dx_max)
dy = np.random.uniform(low=dy_min, high=dy_max)
w = np.random.uniform(low=w_min, high=w_max)

params = np.array([dx, dy, w])

# NOTE: the normalization is hardcoded at the moment
# TODO: package tf model so that this data is easy to keep track of
EIM_coefficients = ml_fits.predict(np.array([params]))[0] * 4.308446821664332e-07

ROM = np.dot(EIM_coefficients, B_matrix).reshape(200, 200)

x, y, z = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.1, 0.1, 5),
)
# %%
plt.contourf(
    x,
    y,
    ROM / 1e-6,
    20,
)
plt.gca().set_aspect("equal")
cb0 = plt.colorbar()
cb0.ax.set_ylabel("Optical path depth [um]")
plt.xlabel("x [m]")
plt.ylabel("y [m]")
plt.title("ROM")
plt.savefig("OPD_ROM.pdf", bbox_inches="tight")
