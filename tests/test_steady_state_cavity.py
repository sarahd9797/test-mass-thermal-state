# %%
from ifo_thermal_state.aligo_3D import (
    make_test_mass_model,
    AdvancedLIGOTestMass3DSteadyState,
)
from ifo_thermal_state.math import composite_newton_cotes_weights

# import matplotlib.pyplot as plt
import numpy as np
from types import SimpleNamespace
import finesse
from finesse.cymath.homs import HGModes
from finesse.knm import Map


# %%
def update_fem():
    ss_itm.temperature.I_HR.interpolate(I_ITM_HR)
    ss_itm.solve_temperature()
    ss_itm.solve_deformation()

    ss_etm.temperature.I_HR.interpolate(I_ETM_HR)
    ss_etm.solve_temperature()
    ss_etm.solve_deformation()


def get_mask(x, y, ss):
    _, _, mask = ss.evaluate_deformation(x, y, 0.1, meshgrid=True)
    return mask.astype(int)[:, :, 0]


def get_deformation(x, y, ss):
    _, S, mask = ss.evaluate_deformation(x, y, 0.1, meshgrid=True)
    S[~mask] = 0
    return S[:, :, 0, 2]


def get_opd(x, y, ss):
    z = np.linspace(-0.1, 0.1, 5)
    xyz, dT, mask = ss.evaluate_temperature(x, y, z, meshgrid=True)
    dT[~mask] = 0
    # Use better quadrature rule for integration so it requires less z points
    # The depth profile typically looks like a polynomial, so a composite
    # newton cotes here should work well
    weights = composite_newton_cotes_weights(z.size, 5)
    # integrate change in temperature over the optical path and compute how
    # much the optical path depth we accumulate
    dz = z[1] - z[0]
    OPD = (
        model.dndT
        * dz
        * np.sum(
            dT[:, :, :, 0] * weights[None, None, :], axis=2
        )  # weight Z direction and sum
    )
    return OPD


# %% Make the ALIGO test mass
model = make_test_mass_model(
    mesh_function_kwargs={
        "HR_mesh_size": 0.02,
        "AR_mesh_size": 0.02,
        "mesh_algorithm": 6,
    }
)

# %% Make a simple fabry perot cavity
ifo = finesse.script.parse(
    """
variable nsilica 1.45
variable Mloss 30u

laser L0 P=2500
s l1 L0.p1 ITMlens.p1
lens ITMlens f=inf
s l2 ITMlens.p2 ITM.p1
m ITM T=0.014 L=Mloss R=1-ITM.L-ITM.T Rc=-1934
s L ITM.p2 ETM.p1 L=3994
m ETM T=5u L=Mloss R=1-ETM.L-ETM.T Rc=2245
cav cavARM ITM.p2.o

fd E_itm ITM.p2.i f=0
fd E_etm ETM.p1.i f=0
pd Pcirc ITM.p2.o
pd Prefl ITM.p1.o
"""
)
ifo.modes("even", maxtem=8)
ifo.beam_trace()  # ABCD trace all the beams through the model
# Set the input laser to be fixed to the currently traced value
# which should be mode matched to the cavity in this state
ifo.L0.p1.o.q = ifo.L0.p1.o.q
ifo.run("pseudo_lock_cavity(cavARM)")  # lock the cavity
out = ifo.run()  # initial run
print("Power circulating", out["Pcirc"])
# %%
# define the x/y points to evaluate the FEA at generate some distortions to load into finesse
x, y = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
)
# Storage object that the FEA update tools will use to upload
values = SimpleNamespace()
values.out = ifo.run()


def I_ITM_HR(x):
    # interpolates some intensity pattern onto a boundary for the
    # HR surface of the ITM
    HGs = HGModes(ifo.ITM.p2.i.q, ifo.homs)
    # evalute the beam shape over the requested points
    a = HGs.compute_points(x[0], x[1]) * values.out["E_itm"][:, None]
    E = np.sum(a, axis=0)  # Total optical field [sqrt{W}/m^2]
    I = E * E.conj()  # Intensity of optical field [W/m^2]
    return I.real * 0.5e-6


def I_ETM_HR(x):
    # interpolates some intensity pattern onto a boundary for the
    # HR surface of the ETM
    HGs = HGModes(ifo.ETM.p1.i.q, ifo.homs)
    a = HGs.compute_points(x[0], x[1]) * values.out["E_etm"][:, None]
    E = np.sum(a, axis=0)
    I = E * E.conj()
    return I.real * 0.5e-6 * 3 / 5


ss_itm = AdvancedLIGOTestMass3DSteadyState(model)
ss_etm = AdvancedLIGOTestMass3DSteadyState(model)

update_fem()

# %%
ifo.ITM.phi.is_tunable = True
ifo.ETM.phi.is_tunable = True
ifo.modes("even", maxtem=10)
ifo.ITM.surface_map = Map(x, y, amplitude=get_mask(x, y, ss_itm))
ifo.ETM.surface_map = Map(x, y, amplitude=get_mask(x, y, ss_etm))
ifo.ITMlens.OPD_map = Map(x, y, amplitude=get_mask(x, y, ss_etm))
ifo.L0.P = 2600
ifo.run("pseudo_lock_cavity(cavARM)")
values.out = ifo.run("noxaxis()")
for i in range(5):
    update_fem()
    ifo.ITM.surface_map = Map(
        x, y, amplitude=get_mask(x, y, ss_itm), opd=-get_deformation(x, y, ss_itm)
    )
    ifo.ETM.surface_map = Map(
        x, y, amplitude=get_mask(x, y, ss_etm), opd=+get_deformation(x, y, ss_etm)
    )
    ifo.ITMlens.OPD_map = Map(
        x, y, amplitude=get_mask(x, y, ss_etm), opd=get_opd(x, y, ss_itm)
    )
    sols = ifo.run("series(pseudo_lock_cavity(cavARM), noxaxis())")
    print(
        np.sum(abs(values.out["E_itm"]) ** 2),
        np.sum(abs(sols["noxaxis"]["E_itm"]) ** 2),
    )
    values.out = sols["noxaxis"]

# %%
