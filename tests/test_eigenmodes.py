# %%
import numpy as np
import ufl
from types import SimpleNamespace
from ifo_thermal_state.mesh import sweep_cylinder_3D
import sys
import slepc4py
from dolfinx.fem import (
    Constant,
    Function,
    VectorFunctionSpace,
    form,
)
from dolfinx.fem.petsc import assemble_matrix
from mpi4py import MPI
from slepc4py import SLEPc
from dolfinx.io import XDMFFile
from dolfinx.plot import create_vtk_mesh
import pyvista

slepc4py.init(sys.argv)

# %%

# Creat a cylinder
cylinder_model = SimpleNamespace()
radius = cylinder_model.radius = 0.17
thickness = cylinder_model.thickness = 0.2

HR_mesh_size = 0.025
AR_mesh_size = 0.025

num_elements = [10]
heights = [1]

(
    cylinder_model.msh,
    cylinder_model.cell_markers,
    cylinder_model.facet_markers,
) = sweep_cylinder_3D(
    radius=radius,
    thickness=thickness,
    num_elements=num_elements,
    heights=heights,
    HR_mesh_size=HR_mesh_size,
    AR_mesh_size=AR_mesh_size,
    mesh_algorithm=6,
)

# %%
rho = cylinder_model.rho = 2202  # density
E = cylinder_model.E = 72e9  # Young's modulus
nu_ = cylinder_model.nu_ = 0.17  # Poisson ratio

# Get Lame coeff
mu = Constant(cylinder_model.msh, cylinder_model.E / 2 / (1 + cylinder_model.nu_))

lambda_ = Constant(
    cylinder_model.msh,
    cylinder_model.E
    * cylinder_model.nu_
    / (1 + cylinder_model.nu_)
    / (1 - 2 * cylinder_model.nu_),
)


# Convenient functions relating strain and stress:
def epsilon(u):
    """Return symmetric stran tensor:"""
    return ufl.sym(ufl.grad(u))


def sigma(u):
    dim = len(u)
    return lambda_ * ufl.nabla_div(u) * ufl.Identity(dim) + 2 * mu * epsilon(u)


# Define function space
V = cylinder_model.V = VectorFunctionSpace(cylinder_model.msh, ("CG", 2))
u = cylinder_model.u = ufl.TrialFunction(V)
v = cylinder_model.v = ufl.TestFunction(V)

# Forms and matrices:
a_form = form(ufl.inner(sigma(u), epsilon(v)) * ufl.dx)
m_form = form(rho * ufl.inner(u, v) * ufl.dx)

A = assemble_matrix(a_form, [])
A.assemble()
M = assemble_matrix(m_form, [])
M.assemble()

# Create and configure eigensolver:
N_eig = 10
eigensolver = SLEPc.EPS().create(cylinder_model.msh.comm)
eigensolver.setDimensions(N_eig)
# Since we expect that the eigenvlaue (frequencies) must be real, this problem is set to Generalised
# Hermitiane eigenvalue proble,
eigensolver.setProblemType(SLEPc.EPS.ProblemType.GHEP)

# Set tolerance for iterative solver:
tol = 1e-9
eigensolver.setTolerances(tol=tol)

# Spectral transform:
# Generally, the SLEPc solver will solve for the largest eigenvalue first, here we want to
# look for the smallest eigenvalue first. A spectral transform is therefore performed with
# SLEPC.ST.Type.SINVERT (SINVERT stands for shift-and_invert)

st = SLEPc.ST().create(cylinder_model.msh.comm)
st.setType(SLEPc.ST.Type.SINVERT)
st.setShift(0)
st.setFromOptions()
eigensolver.setST(st)
eigensolver.setWhichEigenpairs(SLEPc.EPS.Which.TARGET_REAL)
eigensolver.setOperators(A, M)
eigensolver.setFromOptions()

# %%
# Compute eigenvalue-eigenvectors pairs:
# Compute eigenvalue-eigenvector pairs
eigensolver.solve()
evs = eigensolver.getConverged()
vr, vi = A.getVecs()
u_output = Function(V)
u_output.name = "Eigenvector"
print(f"Number of converged eigenpairs {evs}")
u_eigvecs = []
if evs > 0:
    with XDMFFile(MPI.COMM_WORLD, "./data/eigenvectors.xdmf", "w") as xdmf:
        xdmf.write_mesh(cylinder_model.msh)
        for i in range(min(N_eig, evs)):
            l = eigensolver.getEigenpair(i, vr, vi)
            freq = np.sqrt(l.real) / 2 / np.pi
            print(f"Mode {i}: {freq} Hz")
            u_output.x.array[:] = vr
            u_eigvecs.append(u_output.x.array[:].copy())
            xdmf.write_function(u_output, i)
# %%
# Plot eigenmode displacement
p = pyvista.Plotter(notebook=True)
topology, cell_types, geometry = create_vtk_mesh(cylinder_model.V)
grid = pyvista.UnstructuredGrid(topology, cell_types, geometry)
grid["u"] = u_eigvecs[-5].reshape((geometry.shape[0], 3))
p.add_mesh(grid, style="wireframe", color="k")
warped = grid.warp_by_vector("u", factor=1e-1)
p.add_mesh(warped, show_edges=False)
p.show_axes()
p.show(jupyter_backend="trame")


# %%
