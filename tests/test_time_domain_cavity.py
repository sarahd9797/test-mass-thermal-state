# %%
from ifo_thermal_state.aligo_3D import (
    make_test_mass_model,
    AdvancedLIGOTestMass3DTime,
)
from ifo_thermal_state.math import composite_newton_cotes_weights
import matplotlib.pyplot as plt
import numpy as np
from types import SimpleNamespace
import finesse
from finesse.cymath.homs import HGModes
from finesse.knm import Map
import finesse.ligo
import pickle
import gpstime
from pathlib import Path

finesse.init_plotting()


# %%
def get_mask(x, y, ss):
    _, _, mask = ss.evaluate_deformation(x, y, 0.1, meshgrid=True)
    return mask.astype(int)[:, :, 0]


def get_deformation(x, y, ss):
    xyz, S, mask = ss.evaluate_deformation(x, y, 0.1, meshgrid=True)
    S[~mask] = 0
    return S[:, :, 0, 2]


def get_opd(x, y, ss):
    z = np.linspace(-0.1, 0.1, 11)
    xyz, dT, mask = ss.evaluate_temperature(x, y, z, meshgrid=True)
    dT[~mask] = 0
    # Use better quadrature rule for integratiopn
    weights = composite_newton_cotes_weights(z.size, 5)
    dz = z[1] - z[0]
    OPD = (
        model.dndT
        * dz
        * np.sum(
            dT[:, :, :, 0] * weights[None, None, :], axis=2
        )  # weight Z direction and sum
    )
    return OPD


# %%
add_point_absorber = True

model = make_test_mass_model(
    mesh_function_kwargs={
        "HR_mesh_size": 0.02,
        "AR_mesh_size": 0.03,
        "mesh_algorithm": 6,
        "point": (0.016, -0.01) if add_point_absorber else None,
    }
)
# from ifo_thermal_state.plotting import plot_mesh
# plot_mesh(model.msh, model.cell_markers)
# %%
base = finesse.script.parse(
    """
variable nsilica 1.45
variable Mloss 30u

laser L0 P=1500
s l1 L0.p1 ITMlens.p1
lens ITMlens f=inf
s l2 ITMlens.p2 ITM.p1
m ITM T=0.015 L=Mloss R=1-ITM.L-ITM.T Rc=-1940.3
s L ITM.p2 ETM.p1 L=3994.4
m ETM T=5u L=Mloss R=1-ETM.L-ETM.T Rc=2244.2
cav cavARM ITM.p2.o

fd E_itm1 ITM.p1.i f=0
fd E_itm2 ITM.p1.o f=0
fd E_itm3 ITM.p2.i f=0
fd E_itm4 ITM.p2.o f=0

fd E_etm ETM.p1.i f=0

pd Pcirc ITM.p2.o
pd Prefl L0.p1.i
"""
)
#
base.modes("even", maxtem=10)
# Set mode matching to be good in the "hot state"
base.ITMlens.f = 13000
base.beam_trace()
# Set laser to output this traced mode
base.L0.p1.o.q = base.L0.p1.o.q
# Reset to cold state
base.ITMlens.f = np.inf
base.run("pseudo_lock_cavity(cavARM)")
out = base.run()  # initial run
print(out["Pcirc"])

# %%
ifo = base.deepcopy()
x, y = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
)

values = SimpleNamespace()
values.out = None


def I_ITM_HR(x):
    HGs = HGModes(ifo.ITM.p2.i.q, ifo.homs)
    a = HGs.compute_points(x[0], x[1]) * values.out["E_itm3"][:, None]
    E = np.sum(a, axis=0)
    I = E * E.conj()

    if add_point_absorber:
        r = np.sqrt((x[0] - 0.018) ** 2 + (x[1] - 0) ** 2)
        pabs = np.zeros_like(r)
        pabs[r < 0.001] = 300
        return I.real * 0.5e-6 + pabs * I.real * 0.5e-6
    else:
        return I.real * 0.5e-6


def I_ETM_HR(x):
    HGs = HGModes(ifo.ETM.p1.i.q, ifo.homs)
    a = HGs.compute_points(x[0], x[1]) * values.out["E_etm"][:, None]
    E = np.sum(a, axis=0)
    I = E * E.conj()
    return I.real * 0.5e-6 * 3 / 5


ts_itm = AdvancedLIGOTestMass3DTime(model, 60)
ts_etm = AdvancedLIGOTestMass3DTime(model, 60)


def initial_condition(x):
    return np.full((x.shape[1],), 0)


ts_itm.set_initial_condition(initial_condition)
ts_etm.set_initial_condition(initial_condition)
# static profiles
ITM_static = -finesse.ligo.maps.get_test_mass_surface_profile_interpolated("ITM01")(
    x, y
)
ETM_static = +finesse.ligo.maps.get_test_mass_surface_profile_interpolated("ETM13")(
    x, y
)

ifo.L0.P = 2 * 50 / 2
ifo.ITM.phi.is_tunable = True
ifo.ETM.phi.is_tunable = True
ifo.modes(maxtem=10)
ifo.ITM.surface_map = Map(x, y, amplitude=get_mask(x, y, ts_itm), opd=ITM_static)
ifo.ETM.surface_map = Map(x, y, amplitude=get_mask(x, y, ts_etm), opd=ETM_static)
ifo.ITMlens.OPD_map = Map(x, y, amplitude=get_mask(x, y, ts_etm))
ifo.ITMlens.f.value = np.inf
ifo.run("pseudo_lock_cavity(cavARM, lowest_loss=true)")
values.out = ifo.run("noxaxis()")
outs = [values.out]
eigensols = [ifo.run("eigenmodes(cavARM, 0)")]
t = [0]
models = [ifo.deepcopy()]


# %%
while ts_itm.t <= 4000:
    if ts_itm.t == 0:
        ifo.ITMlens.f.value = np.inf
        ts_itm.set_initial_condition(initial_condition)
    if ts_etm.t == 0:
        ts_etm.set_initial_condition(initial_condition)

    if ts_itm.t > 180:
        ifo.L0.P = 25 * 50 / 2
    if ts_itm.t > 180 + 5 * 60:
        ifo.L0.P = 80 * 50 / 2
    if ts_itm.t > 3000:
        ifo.L0.P = 120 * 50 / 2

    ts_itm.temperature.I_HR.interpolate(I_ITM_HR)
    ts_etm.temperature.I_HR.interpolate(I_ETM_HR)
    ts_itm.step()
    ts_etm.step()
    t.append(ts_itm.t)

    ifo.ITM.surface_map = Map(
        x,
        y,
        amplitude=get_mask(x, y, ts_itm),
        opd=ITM_static - get_deformation(x, y, ts_itm),
    )
    ifo.ETM.surface_map = Map(
        x,
        y,
        amplitude=get_mask(x, y, ts_etm),
        opd=ETM_static + get_deformation(x, y, ts_etm),
    )
    ifo.ITMlens.OPD_map = Map(
        x, y, amplitude=get_mask(x, y, ts_etm), opd=get_opd(x, y, ts_itm)
    )

    aix, aiy = ifo.ITM.surface_map.remove_curvatures(ifo.ITM.p2.i.qx.w)
    ifo.ITM.Rcx.value = -1 / (-1 / base.ITM.Rcx - aix)
    ifo.ITM.Rcy.value = -1 / (-1 / base.ITM.Rcy - aiy)

    aex, aey = ifo.ETM.surface_map.remove_curvatures(ifo.ETM.p1.i.qx.w)
    ifo.ETM.Rcx.value = -1 / (-1 / base.ETM.Rcx - aex)
    ifo.ETM.Rcy.value = -1 / (-1 / base.ETM.Rcy - aey)

    ifo.ITMlens.f.value = ifo.ITMlens.OPD_map.get_thin_lens_f(
        ifo.ITMlens.p1.i.qx.w, average=True
    )
    ifo.ITMlens.OPD_map.remove_curvatures(ifo.ITMlens.p1.i.qx.w, mode="average")

    models.append(ifo.deepcopy())
    sols = ifo.run("series(maximize(Pcirc, ETM.phi), noxaxis(), eigenmodes(cavARM, 0))")

    print(
        ts_itm.t,
        sols["noxaxis"]["Pcirc"],
        ifo.ITMlens.f.value,
        ifo.ITM.Rcx.value,
        ifo.ETM.Rcx.value,
    )

    values.out = sols["noxaxis"]
    outs.append(values.out)
    eigensols.append(sols["eigenmode"])

    if values.out["Pcirc"] < 100:
        break  # lockloss
# %%
path = Path("./data/")
path.mkdir(exist_ok=True)
out = path / f"cavity_time_domain_{str(int(gpstime.gpsnow()))}.pkl"
print(out)
with open(out, "wb") as file:
    pickle.dump(
        {
            "eigensols": eigensols,
            "outs": outs,
            "t": t,
            "add_point_absorber": add_point_absorber,
        },
        file,
    )
# %%
plt.plot(t, [out["Pcirc"] for out in outs])
plt.xlabel("Time [s]")
plt.ylabel("Arm power [W]")
plt.grid(True)
# %%
eigensols[0].plot_phase(label="Cold")
eigensols[-1].plot_phase(label="Hot")
plt.grid(True)
plt.legend()
# %%
plt.plot(t[1:], [1 / m.ITMlens.f.value for m in models[1:]])
# %%
plt.plot(t[1:], [-1 / m.ITM.Rcx.value for m in models[1:]])
plt.plot(t[1:], [-1 / m.ITM.Rcy.value for m in models[1:]])
# %%
plt.plot(t[1:], [1 / m.ETM.Rcx.value for m in models[1:]])
plt.plot(t[1:], [1 / m.ETM.Rcy.value for m in models[1:]])
# %%
