# %%
from ifo_thermal_state.aligo_3D import (
    make_test_mass_model,
    AdvancedLIGOTestMass3DTime,
)
import matplotlib.pyplot as plt
from ifo_thermal_state.mesh import sweep_cylinder_3D
from ifo_thermal_state.postprocessing import get_deformation, get_opd
import numpy as np
import finesse
import finesse.ligo
from types import SimpleNamespace
from finesse.cymath.homs import HGModes
from copy import deepcopy

finesse.init_plotting()

# %%
model = make_test_mass_model(
    mesh_function=sweep_cylinder_3D,
    mesh_function_kwargs={
        "num_elements": [10, 5],
        "heights": [0.98, 1],
        "add_flats": True,
        "HR_mesh_size": 0.02,
        "AR_mesh_size": 0.02,
        "mesh_algorithm": 6,
    },
)


# %%
def run_full(plan, HOM, w=[53e-3, 53e-3], xyc=[0.0, 0.0], dT=0.1, N=100, t_end=36000):
    x, y = (
        np.linspace(-0.17, 0.17, N),
        np.linspace(-0.17, 0.17, N),
    )
    print(w, xyc)
    values = SimpleNamespace()
    values.wx = w[0]
    values.wy = w[1]
    values.homs = np.array(HOM, dtype=np.int32)
    values.E = np.zeros(values.homs.shape[0])
    values.E[0] = 1
    values.P = 1
    values.x0 = xyc[0]
    values.y0 = xyc[1]

    def I_ITM_HR(x):
        qx = finesse.BeamParam(w=values.wx, Rc=np.inf)
        qy = finesse.BeamParam(w=values.wy, Rc=np.inf)
        HGs = HGModes((qx, qy), values.homs)
        a = HGs.compute_points(x[0] - values.x0, x[1] - values.y0) * values.E[:, None]
        E = np.sum(a, axis=0)
        I = values.P * E * E.conj()
        return I.real

    def initial_condition(x):
        return np.full((x.shape[1],), 0)

    tsim = AdvancedLIGOTestMass3DTime(model, dT)
    tsim.set_initial_condition(initial_condition)

    result = SimpleNamespace()
    result.x = x
    result.y = y
    result.t = [0]
    result.step = 0
    result.OPDs = []
    result.z_HR = []
    result.wx = w[0]
    result.wy = w[1]
    result.HOM = HOM
    result.dT = dT
    result.N = N
    result.values = [values]

    result.OPDs.append(get_opd(x, y, tsim))
    result.z_HR.append(get_deformation(x, y, tsim))

    while tsim.t <= t_end:
        plan(result.t[-1], result.step, values)
        print(f"{result.t[-1]:.2f}/{t_end:.2f}", end="\r")
        tsim.temperature.I_HR.interpolate(I_ITM_HR)
        result.values.append(deepcopy(values))
        tsim.step()
        result.t.append(tsim.t)
        result.OPDs.append(get_opd(x, y, tsim))
        result.z_HR.append(get_deformation(x, y, tsim))
        result.step += 1

    return result


def make_pulse_response(
    HOM, w=[53e-3, 53e-3], xyc=[0.0, 0.0], dT=60, N=100, t_end=6000
):
    def plan(t, step, values):
        values.P = 1

    return run_full(plan, HOM, w, xyc, dT, N, t_end)


# %%
HOM = [[0, 0]]  # HG00
initial_dT = 1000
theta = [
    53e-3,
    53e-3,
]
pulse = make_pulse_response(
    HOM, w=[theta[0], theta[1]], xyc=[0, 0], dT=initial_dT, t_end=60000
)
x = pulse.x
y = pulse.y
t = pulse.t
P = np.array([values.P for values in pulse.values])
OPD_nm = np.array(pulse.OPDs) / 1e-9  # in nanometers
z_HR_nm = np.array(pulse.z_HR) / 1e-9  # in nanometers

# %%
plt.imshow(OPD_nm[-1], extent=[x[0], x[-1], y[0], y[-1]])
plt.colorbar(label="OPD [nm]")

# %%
plt.plot(t, OPD_nm[:, 50, 50])
plt.xlabel("Time [s]")
plt.ylabel("OPD [nm]")
