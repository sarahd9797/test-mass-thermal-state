# %%
from ifo_thermal_state.fea import evaluate_solution
from ifo_thermal_state.aligo_3D import make_test_mass_model, AdvancedLIGOTestMass3DTime
import numpy as np
from pathlib import Path
from types import SimpleNamespace
import matplotlib.pyplot as plt

# %%
model = make_test_mass_model(
    mesh_function_kwargs={
        "HR_mesh_size": 0.02,
        "AR_mesh_size": 0.02,
        "mesh_algorithm": 0,
    }
)
# %%
values = SimpleNamespace()
values.w = 53e-3
values.dx = 0
values.dy = 0
values.P_coat = 1


def I_HR(x):
    I0 = 2 * values.P_coat / (np.pi * values.w**2)
    return I0 * np.exp(
        -2 * ((x[0] - values.dx) ** 2 + (x[1] - values.dy) ** 2) / (values.w**2)
    )


# %%
ts = AdvancedLIGOTestMass3DTime(model, 100)


def initial_condition(x):
    return np.full((x.shape[1],), 0)


ts.set_initial_condition(initial_condition)

# %%
x, y, z = (
    np.linspace(-0.17, 0.17, 100),
    np.linspace(-0.17, 0.17, 100),
    0.1,
)  # get HR surface

# %%
save_path = Path("figures/time_domain")
save_path.mkdir(parents=True, exist_ok=True)

while ts.t <= 7200:
    print(ts.t, end="\r")
    if ts.t > 3600:
        values.dx = 40e-3
    else:
        values.dx = 0e-3

    ts.temperature.I_HR.interpolate(I_HR)
    ts.step()

    xyz, W, mask = evaluate_solution(
        model.msh, x, y, z, ts.temperature.uh, meshgrid=True
    )
    fig, axs = plt.subplots(1, 1)
    plt.contourf(
        x,
        y,
        W[:, :, 0, 2],
        20,
    )
    plt.gca().set_aspect("equal")
    cb0 = plt.colorbar()
    cb0.ax.set_ylabel("Temperature [K]")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.savefig(save_path / f"t_{ts.t:05}.jpg")
    plt.close()

# use this to make a movie from the frames
# ffmpeg -framerate 10 -pattern_type glob -i '*.jpg' -c:a copy -shortest -c:v
#    libx264 -pix_fmt yuv420p -crf 10 -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" test.mp4

# %%
