# %%
import numpy as np
import ufl
from dolfinx.fem import Function, FunctionSpace, form, Constant
from dolfinx.fem.petsc import (
    assemble_matrix,
    assemble_vector,
    create_vector,
    create_matrix,
)
from ifo_thermal_state.aligo_3D import make_test_mass_model, AdvancedLIGOTestMass3DTime
from types import SimpleNamespace
from petsc4py import PETSc
from scipy.optimize import curve_fit
from ifo_thermal_state.math import composite_newton_cotes_weights
from ifo_thermal_state.fea import evaluate_solution, BoundaryCondition
import matplotlib.pyplot as plt

# %%
model = make_test_mass_model(
    mesh_function_kwargs={
        "add_flats": True,
        "HR_mesh_size": 0.02,
        "AR_mesh_size": 0.02,
        "mesh_algorithm": 0,
    }
)
# %%
values = SimpleNamespace()
values.w = 53e-3
values.dx = 0
values.dy = 0
values.P_coat = 1


def quadWF(xy, S):
    x, y = xy
    return (-S / 2) * (x**2 + y**2)


def I_HR_func(x):
    I0 = 2 * values.P_coat / (np.pi * values.w**2)
    return I0 * np.exp(
        -2 * ((x[0] - values.dx) ** 2 + (x[1] - values.dy) ** 2) / (values.w**2)
    )


# %%
R = 0.17
H = 0.2
rho = 2202  # density [kg/m^3]
kappa = 1.38  # thermal conductivity [W/(m K)]
C = 745  # specific heat [J/(kg K)]
eps = 0.93  # emissivity of fused silica
sigm = 5.670e-8  # Stefan-Boltzmann constnat [W/(m^2 K^4)]
Text = 293.15  # External temperature [K]
dndT = 8.6e-6

# %%
x1, y1, z1 = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.1, 0.1, 5),
)
dz = z1[1] - z1[0]
X, Y = np.meshgrid(x1, y1)
weights = composite_newton_cotes_weights(z1.size, 5)
# %%

# Fixed time step, matrix A is built before the loop and not updated during time stepping
t = 0
T = 3600  # Final time ( 10 hours)
dt = 20
dt = Constant(model.msh, PETSc.ScalarType(dt))

# Define test function space:
V = FunctionSpace(model.msh, ("CG", 2))
u = ufl.TrialFunction(V)
v = ufl.TestFunction(V)


# Create initial condition:
def initial_condition(x):
    return np.full((x.shape[1],), 0)


u_n = Function(V)
u_n.name = "u_n"
u_n.interpolate(initial_condition)


# Boundary conditions:
eta = -4 * eps * sigm * Text**3
ds = ufl.Measure("ds", domain=model.msh, subdomain_data=model.facet_tag)
x = ufl.SpatialCoordinate(model.msh)
I_HR = Function(V)
I_HR.interpolate(I_HR_func)

bar_bc = (eta / kappa) * ufl.inner(u, v) * ds(3)
AR_bc = (eta / kappa) * ufl.inner(u, v) * ds(2)
HR_bc = (eta / kappa) * ufl.inner(u, v) * ds(1)
HR_bc_src = ufl.inner(I_HR / kappa, v) * ds(1)

uh = Function(V)
uh.name = "uh"
uh.interpolate(initial_condition)

f = Constant(model.msh, PETSc.ScalarType(0))

a = (
    u * v * ufl.dx
    + (kappa / (rho * C)) * dt * ufl.dot(ufl.grad(u), ufl.grad(v)) * ufl.dx
    - (kappa / (rho * C)) * dt * (bar_bc + AR_bc + HR_bc)
)
L = (u_n + (1 / (rho * C)) * dt * f) * v * ufl.dx + (kappa / (rho * C)) * dt * HR_bc_src
_a = form(a)
_L = form(L)

A = assemble_matrix(_a)
A.assemble()
b = create_vector(_L)

solver = PETSc.KSP().create(model.msh.comm)
solver.setOperators(A)
solver.setType(PETSc.KSP.Type.CG)
solver.getPC().setType(PETSc.PC.Type.HYPRE)
solver.getPC().setHYPREType("boomeramg")

time_fixed = []
defocus_fixed = []
while t < T:
    t += dt.value
    time_fixed.append(t)
    with b.localForm() as loc_b:
        loc_b.set(0)
    assemble_vector(b, _L)

    solver.solve(b, uh.vector)
    uh.x.scatter_forward()
    u_n.x.array[:] = uh.x.array

    xyz, dT, mask = evaluate_solution(model.msh, x1, y1, z1, uh, meshgrid=True)
    OPD = dndT * dz * np.sum(dT[:, :, :, 0] * weights[None, None, :], axis=2)

    valid = ~(np.isnan(OPD))
    xy = np.vstack((X[valid].ravel(), Y[valid].ravel()))
    p0 = 1e-6
    popt, pcov = curve_fit(quadWF, xy, OPD[valid].ravel(), p0)
    defocus_fixed.append(popt[0])
defocus_fixed = np.asarray(defocus_fixed)
time_fixed = np.asarray(time_fixed)
# %%

# Running with varying time step:
t = 0
T = 3600  # Final time ( 10 hours)
dt0 = 20  # Initial time step
dt1 = 60  # Final time step
T1 = 2000  # time at which time stepping change
dt = Constant(model.msh, PETSc.ScalarType(dt0))

# Define test function space:
V = FunctionSpace(model.msh, ("CG", 2))
u = ufl.TrialFunction(V)
v = ufl.TestFunction(V)


# Create initial condition:
def initial_condition(x):
    return np.full((x.shape[1],), 0)


u_n = Function(V)
u_n.name = "u_n"
u_n.interpolate(initial_condition)


# Boundary conditions:
eta = -4 * eps * sigm * Text**3
ds = ufl.Measure("ds", domain=model.msh, subdomain_data=model.facet_tag)
x = ufl.SpatialCoordinate(model.msh)
I_HR = Function(V)
I_HR.interpolate(I_HR_func)

bar_bc = (eta / kappa) * ufl.inner(u, v) * ds(3)
AR_bc = (eta / kappa) * ufl.inner(u, v) * ds(2)
HR_bc = (eta / kappa) * ufl.inner(u, v) * ds(1)
HR_bc_src = ufl.inner(I_HR / kappa, v) * ds(1)

uh = Function(V)
uh.name = "uh"
uh.interpolate(initial_condition)

f = Constant(model.msh, PETSc.ScalarType(0))

a = (
    u * v * ufl.dx
    + (kappa / (rho * C)) * dt * ufl.dot(ufl.grad(u), ufl.grad(v)) * ufl.dx
    - (kappa / (rho * C)) * dt * (bar_bc + AR_bc + HR_bc)
)
L = (u_n + (1 / (rho * C)) * dt * f) * v * ufl.dx + (kappa / (rho * C)) * dt * HR_bc_src
_a = form(a)
_L = form(L)

# Create matrix rather than assemble matrix
A = create_matrix(_a)
b = create_vector(_L)

solver = PETSc.KSP().create(model.msh.comm)
solver.setOperators(A)
solver.setType(PETSc.KSP.Type.CG)
solver.getPC().setType(PETSc.PC.Type.HYPRE)
solver.getPC().setHYPREType("boomeramg")

time_varied = []
defocus_varied = []
while t < T:
    if t >= T1:
        dt.value = dt1
    t += dt.value
    time_varied.append(t)
    A.zeroEntries()
    assemble_matrix(A, _a, bcs=[])
    A.assemble()
    with b.localForm() as loc_b:
        loc_b.set(0)
    assemble_vector(b, _L)

    solver.solve(b, uh.vector)
    uh.x.scatter_forward()
    u_n.x.array[:] = uh.x.array

    xyz, dT, mask = evaluate_solution(model.msh, x1, y1, z1, uh, meshgrid=True)
    OPD = dndT * dz * np.sum(dT[:, :, :, 0] * weights[None, None, :], axis=2)

    valid = ~(np.isnan(OPD))
    xy = np.vstack((X[valid].ravel(), Y[valid].ravel()))
    p0 = 1e-6
    popt, pcov = curve_fit(quadWF, xy, OPD[valid].ravel(), p0)
    defocus_varied.append(popt[0])

defocus_varied = np.asarray(defocus_varied)
time_varied = np.asarray(time_varied)


# %%
# Only rebuild matrix at time step change:
t = 0
T = 3600  # Final time ( 10 hours)
dt0 = 20  # Initial time step
dt1 = 60  # Final time step
T1 = 2000  # time at which time stepping change
dt = Constant(model.msh, PETSc.ScalarType(dt0))
# Define test function space:
V = FunctionSpace(model.msh, ("CG", 2))
u = ufl.TrialFunction(V)
v = ufl.TestFunction(V)


# Create initial condition:
def initial_condition(x):
    return np.full((x.shape[1],), 0)


u_n = Function(V)
u_n.name = "u_n"
u_n.interpolate(initial_condition)


# Boundary conditions:
eta = -4 * eps * sigm * Text**3
ds = ufl.Measure("ds", domain=model.msh, subdomain_data=model.facet_tag)
x = ufl.SpatialCoordinate(model.msh)
I_HR = Function(V)
I_HR.interpolate(I_HR_func)

bar_bc = (eta / kappa) * ufl.inner(u, v) * ds(3)
AR_bc = (eta / kappa) * ufl.inner(u, v) * ds(2)
HR_bc = (eta / kappa) * ufl.inner(u, v) * ds(1)
HR_bc_src = ufl.inner(I_HR / kappa, v) * ds(1)

uh = Function(V)
uh.name = "uh"
uh.interpolate(initial_condition)

f = Constant(model.msh, PETSc.ScalarType(0))

a = (
    u * v * ufl.dx
    + (kappa / (rho * C)) * dt * ufl.dot(ufl.grad(u), ufl.grad(v)) * ufl.dx
    - (kappa / (rho * C)) * dt * (bar_bc + AR_bc + HR_bc)
)
L = (u_n + (1 / (rho * C)) * dt * f) * v * ufl.dx + (kappa / (rho * C)) * dt * HR_bc_src
_a = form(a)
_L = form(L)
A = create_matrix(_a)
A.zeroEntries()
assemble_matrix(A, _a, bcs=[])
A.assemble()
b = create_vector(_L)

solver = PETSc.KSP().create(model.msh.comm)
solver.setOperators(A)
solver.setType(PETSc.KSP.Type.CG)
solver.getPC().setType(PETSc.PC.Type.HYPRE)
solver.getPC().setHYPREType("boomeramg")

dt_init = dt.value.copy()
time_varied_single = []
defocus_varied_single = []
count = 0
while t < T:
    if t >= T1:
        dt.value = dt1
    t += dt.value
    time_varied_single.append(t)

    if dt_init != dt.value:
        A.zeroEntries()
        assemble_matrix(A, _a, bcs=[])
        A.assemble()
        dt_init = dt.value.copy()

    with b.localForm() as loc_b:
        loc_b.set(0)
    assemble_vector(b, _L)

    solver.solve(b, uh.vector)
    uh.x.scatter_forward()
    u_n.x.array[:] = uh.x.array

    xyz, dT, mask = evaluate_solution(model.msh, x1, y1, z1, uh, meshgrid=True)
    OPD = dndT * dz * np.sum(dT[:, :, :, 0] * weights[None, None, :], axis=2)

    valid = ~(np.isnan(OPD))
    xy = np.vstack((X[valid].ravel(), Y[valid].ravel()))
    p0 = 1e-6
    popt, pcov = curve_fit(quadWF, xy, OPD[valid].ravel(), p0)
    defocus_varied_single.append(popt[0])

defocus_varied_single = np.asarray(defocus_varied_single)
time_varied_single = np.asarray(time_varied_single)


# %%
# Copy aligo_3d method here for testing
t = 0
T = 3600  # Final time ( 10 hours)
dt0 = 20  # Initial time step
dt1 = 60  # Final time step
T1 = 2000  # time at which time stepping change
dt = Constant(model.msh, PETSc.ScalarType(dt0))

# Define test function space:
V = FunctionSpace(model.msh, ("CG", 2))
u = ufl.TrialFunction(V)
v = ufl.TestFunction(V)


# Create initial condition:
def initial_condition(x):
    return np.full((x.shape[1],), 0)


u_n = Function(V)
u_n.name = "u_n"
u_n.interpolate(initial_condition)

# Boundary conditions:
eta = -4 * eps * sigm * Text**3
ds = ufl.Measure("ds", domain=model.msh, subdomain_data=model.facet_tag)
x = ufl.SpatialCoordinate(model.msh)
I_HR = Function(V)

boundary_conditions = {}
boundary_conditions["HR"] = BoundaryCondition(
    "Robin",
    1,
    (
        eta / kappa,
        -I_HR / eta,
    ),
    u,
    v,
    ds,
)
boundary_conditions["AR"] = BoundaryCondition("Robin", 2, (eta / kappa, 0), u, v, ds)
boundary_conditions["BR"] = BoundaryCondition("Robin", 3, (eta / kappa, 0), u, v, ds)

uh = Function(V)
uh.name = "uh"
uh.interpolate(initial_condition)

f = Constant(model.msh, PETSc.ScalarType(0))

F = -u * v * ufl.dx
F += -(kappa / (rho * C)) * dt * ufl.dot(ufl.grad(u), ufl.grad(v)) * ufl.dx
F += (u_n + (1 / (rho * C)) * dt * f) * v * ufl.dx

for condition in boundary_conditions.values():
    F += (kappa / (rho * C)) * dt * condition.bc

_a = form(ufl.lhs(F))
_L = form(ufl.rhs(F))
A = create_matrix(_a)
A.zeroEntries()
assemble_matrix(A, _a, bcs=[])
A.assemble()
b = create_vector(_L)

solver = PETSc.KSP().create(model.msh.comm)
solver.setOperators(A)
solver.setType(PETSc.KSP.Type.CG)
solver.getPC().setType(PETSc.PC.Type.GAMG)

dt_init = dt.value.copy()
I_HR.interpolate(I_HR_func)

time_varied_single_aligo3d = []
defocus_varied_single_aligo3d = []


while t < T:
    if t >= T1:
        dt.value = dt1

    t += dt.value
    time_varied_single_aligo3d.append(t)
    if dt_init != dt.value:
        # A = create_matrix(_a)
        # b = create_vector(_L)
        A.zeroEntries()
        assemble_matrix(A, _a, bcs=[])
        A.assemble()

        dt_init = dt.value.copy()

    with b.localForm() as loc_b:
        loc_b.set(0)
    assemble_vector(b, _L)

    solver.solve(b, uh.vector)
    uh.x.scatter_forward()
    u_n.x.array[:] = uh.x.array

    xyz, dT, mask = evaluate_solution(model.msh, x1, y1, z1, uh, meshgrid=True)
    OPD = dndT * dz * np.sum(dT[:, :, :, 0] * weights[None, None, :], axis=2)

    valid = ~(np.isnan(OPD))
    xy = np.vstack((X[valid].ravel(), Y[valid].ravel()))
    p0 = 1e-6
    popt, pcov = curve_fit(quadWF, xy, OPD[valid].ravel(), p0)
    defocus_varied_single_aligo3d.append(popt[0])

defocus_varied_single_aligo3d = np.asarray(defocus_varied_single_aligo3d)
time_varied_single_aligo3d = np.asarray(time_varied_single_aligo3d)


# %%
# Using method coded up:
t = 0
T = 3600  # Final time ( 10 hours)
dt0 = 20  # Initial time step
dt1 = 60  # Final time step
T1 = 2000  # time at which time stepping change
ts_itm = AdvancedLIGOTestMass3DTime(model, dt0)


def initial_condition(x):
    return np.full((x.shape[1],), 0)


ts_itm.set_initial_condition(initial_condition)
ts_itm.temperature.I_HR.interpolate(I_HR_func)
time_varied_aligo3d = [0]
defocus_varied_aligo3d = [0]

while ts_itm.t < T:
    if ts_itm.t >= T1:
        ts_itm.dt.value = dt1

    ts_itm.step()
    print(ts_itm.t)
    time_varied_aligo3d.append(ts_itm.t)

    xyz, dT, mask = ts_itm.evaluate_temperature(x1, y1, z1, meshgrid=True)
    OPD = dndT * dz * np.sum(dT[:, :, :, 0] * weights[None, None, :], axis=2)
    valid = ~(np.isnan(OPD))
    xy = np.vstack((X[valid].ravel(), Y[valid].ravel()))
    p0 = 1e-6
    popt, pcov = curve_fit(quadWF, xy, OPD[valid].ravel(), p0)
    defocus_varied_aligo3d.append(popt[0])

defocus_varied_aligo3d = np.asarray(defocus_varied_aligo3d)
time_varied_aligo3d = np.asarray(time_varied_aligo3d)
# %%
plt.plot(time_fixed, -defocus_fixed, "b", label="fixed time step")

plt.plot(
    time_varied,
    -defocus_varied,
    "r:",
    label=" varied time step - rebuild matrix every loop",
)
plt.plot(
    time_varied_single,
    -defocus_varied_single,
    "g--",
    label="varied  time step - rebuild matrix once",
)
plt.plot(
    time_varied_aligo3d,
    -defocus_varied_aligo3d,
    "m--",
    label="varied  time step - aligo_3D",
)


plt.xlabel("time")
plt.xlim(0, 3600)
plt.ylabel("S [D]")
plt.legend()
plt.grid()
