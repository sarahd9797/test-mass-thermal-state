# %%
from ifo_thermal_state.aligo_3D import (
    make_test_mass_model,
    AdvancedLIGOTestMass3DSteadyState,
)
from ifo_thermal_state.math import composite_newton_cotes_weights
import matplotlib.pyplot as plt
import numpy as np
from types import SimpleNamespace
import finesse
from finesse.cymath.homs import HGModes

# %%
model = make_test_mass_model(
    mesh_function_kwargs={
        "HR_mesh_size": 0.02,
        "AR_mesh_size": 0.02,
        "mesh_algorithm": 0,
    }
)
# from ifo_thermal_state.plotting import plot_deformation
# plot_mesh(model.msh, model.cell_markers)

# %%
ifo = finesse.script.parse(
    """
l l1 P=1
m m1
link(l1, m1)
modes([[0,0],[3,3]])
fd E m1.p1.i f=0
gauss g1 l1.p1.o w0=50e-3 z=0
"""
)
ifo.l1.tem(3, 3, 1)
ifo.l1.tem(0, 0, 0)
out = ifo.run()
# %%
qx, qy = ifo.m1.p1.i.q
amplitudes = out["E"]
x = y = np.linspace(0, 0.1)
HGs = HGModes((qx, qy), ifo.homs)

# %%
values = SimpleNamespace()
values.P_coat = 1


def I_HR(x):
    a = HGs.compute_points(x[0], x[1]) * amplitudes[:, None]
    E = np.sum(a, axis=0)
    I = E * E.conj()
    return I.real * values.P_coat


ss = AdvancedLIGOTestMass3DSteadyState(model)

# %%
ss.temperature.I_HR.interpolate(I_HR)
ss.solve_temperature()
ss.solve_deformation()
# plot_deformation(ss.deformation.V, ss.deformation.solution)

# %%
x, y, z = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
    [-0.1, 0.1],
)

# %%
xyz, dT, mask = ss.evaluate_temperature(x, y, z, meshgrid=True)

# %%
fig, axs = plt.subplots(1, 2, figsize=(10, 4))

for i, title in zip(range(2), ["AR", "HR"]):
    _mask = mask[:, :, i]
    plt.sca(axs[i])
    plt.contourf(
        x,
        y,
        dT[:, :, i, 0],
        20,
    )
    plt.gca().set_aspect("equal")
    cb0 = plt.colorbar()
    cb0.ax.set_ylabel("Temperature [K]")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.title(title)

plt.tight_layout()

# %%
xyz, S, mask = ss.evaluate_deformation(x, y, z, meshgrid=True)

# %%
fig, axs = plt.subplots(1, 2, figsize=(10, 4))

for i, title in zip(range(2), ["AR", "HR"]):
    _mask = mask[:, :, i]
    plt.sca(axs[i])
    plt.contourf(
        x,
        y,
        1e9 * (S[:, :, i, 2] - S[_mask, i, 2].min()),
        20,
    )
    plt.gca().set_aspect("equal")
    cb0 = plt.colorbar()
    cb0.ax.set_ylabel("Surface deformation [nm]")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.title(title)

plt.tight_layout()

# %% 3D integration
x, y, z = (
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.17, 0.17, 200),
    np.linspace(-0.1, 0.1, 5),
)
# %%
xyz, dT, mask = ss.evaluate_temperature(x, y, z, meshgrid=True)
# %%
# Use better quadrature rule for integratiopn
weights = composite_newton_cotes_weights(z.size, 5)
dz = z[1] - z[0]
OPD = (
    model.dndT
    * dz
    * np.sum(
        dT[:, :, :, 0] * weights[None, None, :], axis=2
    )  # weight Z direction and sum
)

plt.contourf(
    x,
    y,
    OPD / 1e-6,
    20,
)
plt.gca().set_aspect("equal")
cb0 = plt.colorbar()
cb0.ax.set_ylabel("Optical path depth [um]")
plt.xlabel("x [m]")
plt.ylabel("y [m]")
plt.title("Substrate optical path depth")
# %%
